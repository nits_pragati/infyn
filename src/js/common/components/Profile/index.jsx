import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import Sidebar from '../Sidebar';
import { connect } from 'react-redux';
import { actions as loginActions } from '../../../redux/modules/login';
import { loginSelector } from '../../../redux/selectors/loginSelector';
class Profile extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
          initialized: false,
        }
    }

    componentDidMount() {
      let userData;
      if (this.props.login.userData) {
        console.log('User Data  ', this.props.login.userData.toJS())
        userData = this.props.login.userData.toJS();
        this.props.getEditProfile(userData.id);
        this.setState({ initialized: false });
      }
    }


    render() {
      const { editprofile } = this.props;
      let profileDetails;
      if (editprofile.profile && !this.state.initialized) {
        profileDetails = editprofile.profile.toJS();
       profileDetails = profileDetails.data;
     }
        return (
            <div>
              <section className="py-4">
                    <div className="container">
			            <div className="row">
                            <Sidebar />
                            <div className="col-md-9">
                              {profileDetails?(
                                <div className="card dashboard-panel">
                                    <div className="card-header">My Profile</div>
                                    <div className="card-body my-dahsboard">
                                        <div className="media">
                                            <img className="align-self-center mr-3" src={profileDetails.profileImg?__CONFIG__.user_image + profileDetails.profileImg:require('../../../../assets/img/profile.png')} alt="Generic placeholder image" />
                                            <div className="media-body align-self-center">
                                                <h6 className="mt-0">{profileDetails.firstname} {profileDetails.lastname}</h6>
                                                <p>{profileDetails.email} <br/>{profileDetails.phoneNumber}</p>
                                            </div>
                                            <div className="media-right align-self-center">
                                            <Link to="/editprofile">  <button type="button" className="btn btn-light"><i className="fa fa-edit"></i> Edit</button> </Link>
                                            </div>
                                        </div>
                                        <h5 className="my-4">Profile Infomations</h5>
                                        <div className="profile-information">

                                            <p>
                                                <b>First Name:</b>
                                                <span>
                                                    {profileDetails.firstname}
                                                </span>
                                            </p>
                                            <p>
                                                <b>Last Name:</b>
                                                <span>
                                                    {profileDetails.lastname}
                                                </span>
                                            </p>
                                            <p>
                                                <b>Gender:</b>
                                                <span>
                                                    {profileDetails.gender=='M'?'Male':'Female'}
                                                </span>
                                            </p>
                                            <p>
                                                <b>Birth Date:</b>
                                                <span>
                                                    {profileDetails.birthDate}
                                                </span>
                                            </p>
                                            <p>
                                                <b>Mobile:</b>
                                                <span>
                                                    {profileDetails.phoneNumber}
                                                </span>
                                            </p>
                                            <p>
                                                <b>Address:</b>
                                                <span>
                                                    {profileDetails.address.address} {profileDetails.address.addressLine2}<br/>
                                                    {profileDetails.address.city}<br/>
                                                    {profileDetails.address.state}<br/>
                                                    {profileDetails.address.country}, {profileDetails.address.zipCode}<br/>
                                                </span>
                                            </p>

                                        </div>
                                    </div>
                                </div>
                              ):''}
                            </div>
                        </div>
                    </div>
                </section>
            </div>
           );
    }
}
const mapDispatchToProps = {
  ...loginActions,
};
const mapStateToProps = state => ({
  login: loginSelector(state),
});
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
