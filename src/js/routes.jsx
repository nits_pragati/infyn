import React from 'react';
import {
  Route,
  Switch,
  withRouter,
} from 'react-router-dom';
import { Header } from './common/components/Header';
import { Footer } from './common/components/Footer';
import EditProfileView from './views/editprofile';
import HomeView from './views/home';
import AboutView from './views/about';
import Signup from './views/signup';
import Login from './views/login';
import ForgotPasswordView from './views/forgotpassword';
import Profile from './views/profile';
import Dashboard from './views/dashboard';
import HelpView from './views/help';
import ApplyNowView from './views/apply-now';
import ApplicationView from './views/application';

/** import organ campaign */
import { NotificationContainer } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import 'axios-progress-bar/dist/nprogress.css';
import ExampleRouteHandler from './views/example';
import { loadProgressBar } from 'axios-progress-bar';
import { BusinessView} from './views/business';

loadProgressBar();

// import '../assets/fonts/fonts.css';

const JustAnotherPage = () => (
  <div>
    <h2>This is Just Another Page</h2>
    <p>Please remove this from your route, it is just to show case basic setup for router.</p>
  </div>
);

const HeaderWithRouter = withRouter(props => <Header {...props} />);
const FooterWithRouter = withRouter(props => <Footer {...props} />);

module.exports = (
  <div>
    <HeaderWithRouter />
    <NotificationContainer />
    <Switch>
      <Route exact path="/" component={HomeView} />
      {/* <Route path="/signup" component={Signup} />
      <Route path="/login" component={Login} />
      <Route path="/forgotpassword" component={ForgotPasswordView} />
      <Route path="/editprofile" component={EditProfileView} />
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/profile" component={Profile} /> */}
      <Route path="/about" component={AboutView}/>
      <Route path="/help" component={HelpView}/>
      <Route path="/applynow" component={ApplyNowView}/>
      <Route path="/application" component={ApplicationView}/>
      <Route path="/business" component={Dashboard}/>
      <Route path="/ex" component={ExampleRouteHandler} />
      <Route path="*" component={ExampleRouteHandler} />
    </Switch>
    <FooterWithRouter />
  </div>
);
