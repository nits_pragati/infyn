import axios from "axios";

export function submitProfile(userId,bodyFormData){
  return axios({
        method: "POST",
        config: { headers: {'Content-Type': 'multipart/form-data' }},
        url:__CONFIG__.api_url + "user/" + userId + "/edit",
        data: bodyFormData
    })
}

export function getSignupApi(data)
{
    // var bodyFormData = new FormData();
    // var req_data = data.data;
    // for (var key in req_data) {
    //   if(key=='address' || key=='userAuth')
    //   {
    //     for(var k in req_data[key])
    //     {
    //       bodyFormData.append(key + '.' + k, req_data[key][k]);
    //     }
    //   }
    //   else {
    //     bodyFormData.append(key, req_data[key]);
    //   }
    //
    //
    //
    // }
    // console.log('SIGNUP API BY FORM DATA BY SUMAN',bodyFormData);
  //__CONFIG__.api_url + "country"
  return axios({
        method: "POST",
        config: { headers: {'Content-Type': 'multipart/form-data' }},
        url:__CONFIG__.api_url + "user/add",
        data: data
    })
}


export function getForgotpasswordApi(data)
{
  return axios({
        method: "POST",
        //headers: { "Content-type": "application/json" },
        config: { headers: {'Content-Type': 'multipart/form-data' }},
        url:__CONFIG__.api_url + "auth/forgetpassword",
        data: data
    })
}




export function getSubTopicApi(data)
{
  console.log('hh', data);
  return axios({
        method: "GET",
        headers: { "Content-type": "application/json" },
        //config: { headers: {'Content-Type': 'multipart/form-data' }},
        url:__CONFIG__.api_url + "topic/"+data,
        //data: data
    })
    
}



export function getUserVerificationApi(data)
{
    console.log('VVV', data);
  return axios({
        method: "POST",
        //headers: { "Content-type": "application/json" },
        config: { headers: {'Content-Type': 'multipart/form-data' }},
        url:__CONFIG__.api_url + "user/verification/update",
        data: data
    })
}




export function becomeDonorApi(userId,data)
{
  console.log('donor api in API Section', data);
  return axios({
        method: "POST",
        headers: { "Content-type": "application/json" },
        //config: { headers: {'Content-Type': 'multipart/form-data' }},
        url:__CONFIG__.api_url + "user/" + userId + "/donor/update",
        data: data
    })


}





export function getOrganApi(){
    return axios({
          method: "GET",
          headers: { "Content-type": "application/json" },
          //config: { headers: {'Content-Type': 'multipart/form-data' }},
          url:__CONFIG__.api_url + "healthtype/list",
      })
  }

export function getUserDetails(id){
    return axios({
        method: "GET",
        headers: { "Content-type": "application/json" },
        url: __CONFIG__.api_url + "user/" + id,
    })
}