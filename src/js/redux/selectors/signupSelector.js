import { createSelector } from 'reselect';

const signupDataSelector = state => state.signup;

const resultSelector = createSelector(
  signupDataSelector,
  payload => payload.get('countries'),

);

const citySelector = createSelector(
  signupDataSelector,
  payload => payload.get('cities'),

);

const stateSelector = createSelector(
  signupDataSelector,
  payload => payload.get('states'),

);

const signRetSel = createSelector(
  signupDataSelector,
  payload => payload.get('signup_return'),

);

const signupErrorSelector = createSelector(
  signupDataSelector,
  payload => payload.get('signup_error')
);


export const signupSelector = state => ({
  countries: resultSelector(state),
  cities:citySelector(state),
  states:stateSelector(state),
  signup_return:signRetSel(state),
  signup_error:signupErrorSelector(state),
});
