import { put, fork, takeLatest, call } from 'redux-saga/effects';
import { constants as editprofileConstants, actions as editprofileActions } from '../modules/editprofile';

import type { editprofileType } from '../../common/types/editprofile';

import axios from "axios";



function getProfileApi(payload)
{
  //__CONFIG__.api_url + "country"
  //console.log('state id ', params.id);
  return axios({
        method: "get",
        url:__CONFIG__.api_url + "user/" + payload.userId,
    }).then(response => response.data
    );
}

export function* getProfileList(actions) {
  try {
          const response = yield call(getProfileApi,actions.payload);
          //const dog = response.data.message;
          console.log('checking profile by Suman',response);
          // dispatch a success action to the store with the new dog
          yield put(editprofileActions.setEditProfile(response));

      } catch (error) {
          // dispatch a failure action to the store with the error
          //yield put({ type: "API_CALL_FAILURE", error });
          console.log('checking profile Error by Suman');
          yield put(editprofileActions.setEditProfile(error));
      }

}







//////////////////////Country Section

function getCountriesApi()
{
  //__CONFIG__.api_url + "country"
  return axios({
        method: "get",
        url:__CONFIG__.api_url + "country"
    }).then(response => response.data
    );
}


export function* getCountryList() {
  console.log('checking sync ');
  try {
          const response = yield call(getCountriesApi);
          //const dog = response.data.message;
          console.log('checking sync ',response);
          // dispatch a success action to the store with the new dog
          yield put(editprofileActions.setCountries(response));

      } catch (error) {
          // dispatch a failure action to the store with the error
          //yield put({ type: "API_CALL_FAILURE", error });
          console.log('checking sync 1');
          yield put(editprofileActions.setCountries(error));
      }

}

//////////////////////Country Section END




//////////////////////State Section

function getStatesApi(params)
{
  //__CONFIG__.api_url + "country"
  console.log('state id ', params.id);
  return axios({
        method: "get",
        url:__CONFIG__.api_url + "state?countryId=" + params.id,
    }).then(response => response.data
    );
}

export function* getStateList(actions) {
  try {
          const response = yield call(getStatesApi,actions.payload);
          //const dog = response.data.message;
          console.log('checking states ',response);
          // dispatch a success action to the store with the new dog
          yield put(editprofileActions.setStateList(response));

      } catch (error) {
          // dispatch a failure action to the store with the error
          //yield put({ type: "API_CALL_FAILURE", error });
          console.log('checking states 1');
          yield put(editprofileActions.setStateList(error));
      }

}

//////////////////////State Section END



//////////////////////City Section

function getCitiesApi(params)
{
  //__CONFIG__.api_url + "country"
  console.log('state id ', params.id);
  return axios({
        method: "get",
        url:__CONFIG__.api_url + "city?stateId=" + params.id,
    }).then(response => response.data
    );
}

export function* getCityList(actions) {
  try {
          const response = yield call(getCitiesApi,actions.payload);
          //const dog = response.data.message;
          console.log('checking states ',response);
          // dispatch a success action to the store with the new dog
          yield put(editprofileActions.setCityList(response));

      } catch (error) {
          // dispatch a failure action to the store with the error
          //yield put({ type: "API_CALL_FAILURE", error });
          console.log('checking states 1');
          yield put(editprofileActions.setCityList(error));
      }

}

//////////////////////City Section END



function* watchGetEditprofile() {
  yield takeLatest(editprofileConstants.GET_EDITPROFILE, getProfileList);

  yield takeLatest(editprofileConstants.GET_COUNTRIES, getCountryList);
  yield takeLatest(editprofileConstants.GET_STATELIST, getStateList);
  yield takeLatest(editprofileConstants.GET_CITYLIST, getCityList);
}

export const editprofileSaga = [
  fork(watchGetEditprofile),
];
