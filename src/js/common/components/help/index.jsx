import React, { PureComponent } from 'react';
import { postHelpUs } from '../../api/commonApi';
import { NotificationManager } from 'react-notifications';
class Help extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            content: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        var change = {};
        change[event.target.name] = event.target.value;
        this.setState(change);
    }
    handleSubmit(event) {
        debugger;
        if (this.state.first_name && this.state.last_name && this.state.email && this.state.content) {
            postHelpUs(this.state).then((res) => {
                NotificationManager.success("Thank you for submitting your query. We will contact you shortly.", "Success")
                
                this.setState({
                    first_name: '',
                    last_name: '',
                    email: '',
                    content: ''
                })
            }).catch((err) => {
                NotificationManager.error("Please try again later.", "Error");
                
            })
        }
        else {
            NotificationManager.error("All fields are required.", "Error");
        }
        event.preventDefault();
    }
    render() {
        return (
            <div>
                <section className="inner-banner aboutClass">

                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9 col-md-9 ml-auto mr-auto">
                                <div className="home-banner-text">

                                    <img src={require('../../../../assets/img/help-icon.png')} />
                                    <h2>About Us</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div className="help-bg">
                    <div className="inner-body py-4">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-2">
                                </div>
                                <div className="col-md-8 col-12">
                                    <div className="help-form">
                                        <form className="my-4" onSubmit={this.handleSubmit}>
                                            <h2>We’d like to hear from you</h2>
                                            <p>Have a question, inquiry, or feedback for us? Just jot it down and we’ll email you back as soon as possible.</p>
                                            <div className="row mt-4 form">
                                                <div className="col-md-6">
                                                    <div className="form-group">
                                                        <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="First Name" name="first_name" onChange={this.handleChange.bind(this)} value={this.state.first_name} required />
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="form-group">
                                                        <input type="text" className="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" placeholder="Last Name" name="last_name" onChange={this.handleChange.bind(this)} value={this.state.last_name} required />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <input type="email" className="form-control" id="exampleInputPassword3" placeholder="Email" name="email" onChange={this.handleChange.bind(this)} value={this.state.email} required />
                                            </div>
                                            <div className="form-group">
                                                <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" name="content" onChange={this.handleChange.bind(this)} value={this.state.content} required>How can we help?</textarea>
                                            </div>
                                            <button type="submit" className="btn-primery">Submit</button>
                                        </form>
                                    </div>
                                </div>
                                <div className="col-md-2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




        )
    }
}


export default Help;
