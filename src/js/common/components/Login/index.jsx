import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
//import DatePicker from 'react-bootstrap-date-picker'
//import DatePicker from '../../../../../node_modules/react-bootstrap-date-picker/src/index.jsx';
import {NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

class Login extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password : '',
            username : ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        const { login } = this.props;
        const result = login && login.result ? login.result : null;
        if(result && result.size > 0)
        {
            console.log('This is check by Suman');
            console.log('to js ',result.toJS());
        }
        this.props.isLoggedIn()


        //My CHanges

        // console.log('login home ',login);
        // if(this.props.login_error = false)
        // {
        //     console.log('errrrrrrrrr')
        //     NotificationManager.success('Success message', 'Title here');
        // } else {
        //     console.log('GGG')
        //     NotificationManager.warning('Warning message', 'Error here');
        // }

        //My Changes



    }

    handleChange(event) {
        var change = {};
        change[event.target.name] = event.target.value;
        this.setState(change);
    }



    handleSubmit(event) {
        console.log(this.state);
        //NotificationManager.success('Success message', 'Title here');
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        let resultData = {
            "email": "",
            "password": this.state.password,
            "username": ""
          };
          if(reg.test(this.state.email))
          {
            resultData.email = this.state.email;
          }
          else {
            resultData.username = this.state.email;
          }

//console.log(resultData)

     this.props.submitLogin(resultData);
      //console.log("bikash result",test)
        event.preventDefault();


    }

    render() {
      console.log("login data ",this.props.login)
      if(this.props.login.userData)
      {
        this.props.history.push("/");
      }
        return (
            <div>
                <section className="banner-signup d-flex" style={{ backgroundImage: "url(" + require('../../../../assets/img/signup-banner.jpg') + ")" }}>
                    <div className="container align-self-center">
                        <h2 className="text-white">Sign In </h2>
                    </div>
                </section>
                <section className="py-5">
              		<div className="container text-center">
              			<h2 className="font-weight-bold black-text">Member Sign In</h2>
              			<p className="black-text my-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br />Lorem Ipsum has been the industry's standard dummy.</p>
              			<div className="row justify-content-center mt-5 login">
              				<div className="col-md-10">
              					<div className="card p-5 text-left">
              					  <div className="row">
              							<div className="col-lg-6 col-sm-12">
              								<b className="register-text mb-3 d-block">Sign In</b>
                              {this.props.login.login_error ? <p style={{ color: 'red' }}>Login Error</p> : ''}
              								<form onSubmit={this.handleSubmit}>

              							  	<div className="input-group mb-3">
              									  <div className="input-group-prepend">
              									    <img src={require('../../../../assets/img/username.png')} alt="" />
              									  </div>
              									  <input type="text" className="form-control" placeholder="Email" aria-label="Username" aria-describedby="basic-addon1" name="email" onChange={this.handleChange.bind(this)} value={this.state.email} required />
              									</div>
              								 	<div className="input-group mb-3">
              									  <div className="input-group-prepend">
              									    <img src={require('../../../../assets/img/password.png')} alt="" />
              									  </div>
              									  <input type="password" className="form-control" placeholder="Password" aria-label="Username" aria-describedby="basic-addon1" name="password" onChange={this.handleChange.bind(this)} value={this.state.password} required />
              									</div>

              								  <button type="submit" className="btn btn-primary btn-raised">Submit</button>

                              <Link to="/forgotpassword" style={{float: "right"}}>
                                Forgot Password?
                              </Link>

              								</form>

              							</div>
              							<div className="col-lg-1 col-sm-12">
              								<div className="vr">
              									<b>Or</b>
              								</div>
              							</div>
              							<div className="col-lg-5 text-center login-right-social col-sm-12">
              								<img src={require('../../../../assets/img/color_logo.png')} alt="" className="img-fluid"/>
              								<ul className="my-4 mx-auto d-flex list-unstyled justify-content-center ">
              									<li className="facebook"><i className="fa fa-facebook"></i></li>
              									<li className="google"><i className="fa fa-google-plus"></i></li>
              								</ul>
              								<a className="d-block">Don’t have an account?</a>

                              <Link to="/signup">
              								<button type="submit" className="btn btn-outline-primary mt-3">Create an Account</button>
                              </Link>
                              

              							</div>
              						</div>
              					</div>
              				</div>
              			</div>
              		</div>
              	</section>

            </div>
        );
    }
}

export default Login;
