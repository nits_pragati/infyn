import { put, fork, takeLatest, call } from 'redux-saga/effects';
import { constants as forgotpasswordConstants, actions as forgotpasswordActions } from '../modules/forgotpassword';

import type { forgotpasswordType } from '../../common/types/forgotpassword';

import axios from "axios";



function getForgotPasswordApi(data)
{

  console.log('SUMAN FORGOT PASSWORD',data);
  return axios({
        method: "POST",
        //url: "https://dog.ceo/api/breeds/image/random"
        headers: { "Content-type": "application/json" },
        url:__CONFIG__.api_url + "auth/forgetpassword",
        data: data.data
    }).then(response => response.data
  );

}

export function* getForgotPasswordList(actions) {
  console.log('FORGOT PORTION',actions);
  try {
        const response = yield call(getForgotPasswordApi,actions.payload);
        //const dog = response.data.message;
        console.log('checking forgot password ',response);
        // dispatch a success action to the store with the new dog
        yield put(forgotpasswordActions.ForgotPasswordReturn(response));
        

      } catch (error) {
          // dispatch a failure action to the store with the error
          //yield put({ type: "API_CALL_FAILURE", error });
          console.log('checking forgot password error 1');
          yield put(forgotpasswordActions.ForgotPasswordReturn(error));
          
      }

}




function* watchGetForgotPassword() {
  //yield takeLatest(forgotpasswordConstants.GET_FORGOTPASSWORD, fetchHomeData);
  
  yield takeLatest(forgotpasswordConstants.SUBMIT_FORGOTPASSWORD, getForgotPasswordList);
}

export const forgotpasswordSaga = [
  fork(watchGetForgotPassword),
];
