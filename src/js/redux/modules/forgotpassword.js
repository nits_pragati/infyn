import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import type { forgotpasswordType } from '../../common/types/forgotpassword';


const SAVE_FORGOTPASSWORD = 'app/forgotpassword/SAVE_FORGOTPASSWORD';
const GET_FORGOTPASSWORD = 'app/forgotpassword/GET_FORGOTPASSWORD';

const SUBMIT_FORGOTPASSWORD = 'app/forgotpassword/SUBMIT_FORGOTPASSWORD';
const FORGOTPASSWORD_RETURN = 'app/forgotpassword/FORGOTPASSWORD_RETURN';

//const UPDATE_FORGOTPASSWORD = 'app/forgotpassword/UPDATE_FORGOTPASSWORD';

export const constants = {

  GET_FORGOTPASSWORD,
  SAVE_FORGOTPASSWORD,
  SUBMIT_FORGOTPASSWORD,
  FORGOTPASSWORD_RETURN,

};

//export const getForgotPassword = createAction(GET_FORGOTPASSWORD, () => ({}));
//export const updateForgotPassword = createAction(UPDATE_FORGOTPASSWORD, (result : forgotpasswordType) => ({ result }));

export const getForgotPassword = createAction(GET_FORGOTPASSWORD, () => ({}));
export const saveForgotPassword = createAction(SAVE_FORGOTPASSWORD, (result: forgotpasswordType) => ({ result }));

export const submitForgotPassword = createAction(SUBMIT_FORGOTPASSWORD, (data:any) => ({data}));
export const ForgotPasswordReturn = createAction(FORGOTPASSWORD_RETURN, (result: forgotpasswordType) => ({ result }));



export const actions = {
  //getForgotPassword,
  //updateForgotPassword

  getForgotPassword,
  saveForgotPassword,
  submitForgotPassword,
  ForgotPasswordReturn,
};
export const initialState = () =>
  Map({
    result: '',
  })

export const reducers = {
  [SAVE_FORGOTPASSWORD]: (state, { payload }) =>
  state.merge({
    ...payload,
  }),
  [FORGOTPASSWORD_RETURN]: (state, { payload }) =>
    state.merge({
      ...payload,
    }),
    [SUBMIT_FORGOTPASSWORD]: (state, { payload }) =>
          state.merge({
            ...payload,
          }),
}

export default handleActions(reducers, initialState());
