import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { actions as signupActions } from '../../../../redux/modules/signup';
import { signupSelector } from '../../../../redux/selectors/signupSelector';
import _ from 'lodash';
import { getUserDetails } from '../../../api/usersApi';

import { jsonToFormData } from '../../../entity/jsonToFormData';
import { getUserVerificationApi } from '../../../api/usersApi';
import { NotificationManager } from 'react-notifications';


const mapDispatchToProps = {
  ...signupActions,
};
const mapStateToProps = state => ({
  verification: signupSelector(state),
});
@connect(mapStateToProps, mapDispatchToProps)


class VerificationForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      id: '',
      firstname: '',
      lastname: '',
      gender: '',
      userVerifications: '',

      passportFiles: [],
      drivingLicenseFiles: [],
      profileImage: [],

      countries: [],
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.setGender = this.setGender.bind(this);
  }


  setGender(event) {
    console.log(event.target.value);
    this.setState({
      gender: event.target.value,
    })
  }

  

  passportChangedHandler(i, event) {

    console.log('Passport Files ',event.target.files);
    console.log('Passport File ',event.target.files['0']);

    const passFiles = this.state.passportFiles;
    passFiles[i] = event.target.files['0'];
    this.setState({ passportFiles: passFiles })

    passoutput.src = URL.createObjectURL(event.target.files[0]);
  }




    fileChangedHandler = (event) => {
        console.log('Files ',event.target.files);
        console.log('File ',event.target.files['0']);
        var bodyFormData = new FormData();
        for (var x = 0; x < event.target.files.length; x++) {
            bodyFormData.append("profileImage[" + x + "]",event.target.files[x]);
        }
        this.setState({profileImage:event.target.files['0']})
    
    
        output.src = URL.createObjectURL(event.target.files[0]);
    }


  
  //////////////////////////////////// For Selfie Photo Change Upload

  selfieChangedHandler(i, event) {
    
        console.log('Files ',event.target.files);
        console.log('File ',event.target.files['0']);
    
        const selfieFiles = this.state.profileImage;
        selfieFiles[i] = event.target.files['0'];
        this.setState({ profileImage: selfieFiles })
    
        selfieoutput.src = URL.createObjectURL(event.target.files[0]);
      }

  // ////////////////////////////////// For Selfie Photo Change Upload


  driverLicenseChangedHandler(i,event){
    const passFiles = this.state.drivingLicenseFiles;
    passFiles[i] = event.target.files['0'];
    this.setState({ drivingLicenseFiles: passFiles })

    driveroutput.src = URL.createObjectURL(event.target.files[0]);
  }



    handleChange(event) {
      let change = {};
      change[event.target.name] = event.target.value;
      this.setState(change);
    }

    handleSubmit(event) {
      console.log(this.state);


      
        let resultData= {
            "id": "54999",
            "firstname": this.state.firstname,
            "lastname": this.state.lastname,
            "gender": this.state.gender,
            
             "userVerifications.country": 'India',

             "userVerifications.idType": "passport",
             "userVerifications.idNumber": this.state.passporttype,

              passportFiles: this.state.passportFiles,
              drivingLicenseFiles: this.state.drivingLicenseFiles,
              profileImage: this.state.profileImage,
      }

      console.log('VRVR', resultData);


    getUserVerificationApi(jsonToFormData(resultData,{})).then(result => {
        NotificationManager.success('Your profile verified successfully', 'Success');
        //this.props.history.push("/login");

      this.onSkipVerification()
        //this.setState({ currentStep: 3 });
        this.forceUpdate();


    },error=>{
        NotificationManager.error('error!!!', error.response.data.message);
        // console.log(' 7777777777777 ',error.response)
      })

      event.preventDefault();
      event.stopPropagation();
    }

   
    onSkipVerification() {
      this.props.onSkipVerification();
    }

    componentDidMount(){
      this.props.getCountries();
      const userData = JSON.parse(localStorage.getItem('userData'));
      if (userData)
      {
        getUserDetails(userData.id).then(res => {
          const result = res.data.data;
          console.log(" ==== ",result)
          this.state.firstname = result.firstname;
          this.state.lastname = result.lastname;
          this.state.gender = result.gender;
          this.state.country = result.address.country;
          this.forceUpdate()
        })
        
      }
    }


    render() {

      // console.log('LOCAL GET VALUE',localStorage.getItem('lastname'));

      // this.state.firstname = JSON.parse(localStorage.getItem('firstname'));
      // this.state.lastname = JSON.parse(localStorage.getItem('lastname'));

      // const cachedHits = localStorage.getItem(VerificationData);
      // if (cachedHits) {
      //   this.setState({
      //     ss: JSON.parse(cachedHits)
      //   });
      //   return;
      //   console.log('ZZ',this.state.ss);
      // }


      const { verification } = this.props;
      if (verification.countries) {
        this.state.countries = verification.countries.toJS();
      }
      console.log('new home ', verification);


      return (
          <div className="card dashboard-panel">
             <div className="card-header">Verification</div>
             <div className="card-body">
                                      <form className="signup" onSubmit={this.handleSubmit}>
                                          <div className="pertition-box">
                                              <aside>Primary Identification</aside>
                                              <div className="form-group row">
                                                  <label className="bmd-label-static col-sm-3">First Name</label>
                                                  <input type="text" className="form-control col-sm-5" name="firstname" onChange={this.handleChange.bind(this)} value={this.state.firstname} placeholder="Enter your First Name" />
                                                </div>
                                              <div className="form-group row">
                                                  <label className="bmd-label-static col-sm-3">Last Name</label>
                                                  <input type="text" className="form-control col-sm-5" name="lastname" onChange={this.handleChange.bind(this)} value={this.state.lastname} placeholder="Enter your Last Name" />
                                                </div>


                                              <div className="form-group row">
                                                <label className="bmd-label-static col-sm-3">Gender</label>

                                                <div className="col-sm-9 p-0" onChange={this.setGender.bind(this)}>
                                                    <div className="form-check form-check-inline float-left">
                      <input className="form-check-input" type="radio" id="inlineCheckbox1" value="M" onChange={this.handleChange.bind(this)} checked={this.state.gender == 'M'} name="gender" />
                                                        <label className="form-check-label">Male</label>
                                                      </div>
                                                    <div className="form-check form-check-inline float-left">
                      <input className="form-check-input" type="radio" id="inlineCheckbox1" value="F" checked={this.state.gender == 'F'} onChange={this.handleChange.bind(this)} name="gender" />
                                                        <label className="form-check-label">Female</label>
                                                      </div>
                                                  </div>

                                              </div>


                                              <div className="form-group row">
                                                  <label className="bmd-label-static col-sm-3">Country</label>
                                                  <select className="form-control col-sm-5" name="country" onChange={this.handleCountryChange} required>
                                                      <option>Select Country</option>
                                                      {this.state.countries && this.state.countries.data ? this.state.countries.data.map((data, key) => {
                                                        return <option key={key} value={data.countryId} selected={this.state.country==data.name}>{data.name}</option>
                                                      }):''}
                                                    </select>
                                                  {/* <input type="text" class="form-control col-sm-5" id="exampleInputEmail1" value="Kolkata" placeholder="Enter your City"/> */}
                                                </div>
                                            </div>
                                          <div className="pertition-box">
                                              <aside>Passport Verification</aside>
                                              <div className="form-group row">
                                                  <label className="bmd-label-static col-sm-3">Passport Number</label>
                                                  <input type="text" className="form-control col-sm-5" id="exampleInputEmail1" name="passporttype" onChange={this.handleChange.bind(this)} value={this.state.passporttype} placeholder="Enter Passport Number" />
                                                </div>
                                              <div className="form-group row">
                                                  <label className="bmd-label-static col-sm-3">Passport Cover</label>
                                                  <div className="custom-file col-sm-5">
                    <input className="custom-file-input" id="validatedCustomFile" required="" type="file" onChange={this.passportChangedHandler.bind(this,0)}/>
                                                      <label className="custom-file-label" >Upload photos</label>
                                                    </div>
                                                    <div className="col-sm-2 upload-image-box">
                                                      <img id="passoutput" className="img-fluid" src={require('../../../../../assets/img/info.jpg')} />
                                                    </div>
                                                </div>
                                              {/* <div className="form-group row">
                                                  <label className="bmd-label-static col-sm-3">Passport Photo Page</label>
                                                  <div className="custom-file col-sm-5">
                    <input className="custom-file-input" id="validatedCustomFile" onChange={this.passportChangedHandler.bind(this, 1)} required="" type="file" />
                                                      <label className="custom-file-label" >Upload photos</label>
                                                    </div>
                                                    <div className="col-sm-2 upload-image-box">
                                                      <img className="img-fluid" src={require('../../../../../assets/img/info.jpg')} />
                                                    </div>
                                                </div> */}


                                              <div className="form-group row">
  <label className="bmd-label-static col-sm-3">Selfie Photo</label>
  <div className="custom-file col-sm-5">
      <input className="custom-file-input" id="validatedCustomFile" required="" type="file" onChange={this.selfieChangedHandler.bind(this,0)} />
      <label className="custom-file-label" >Upload photos</label>
    </div>
    <div className="col-sm-2 upload-image-box">
      <img className="img-fluid" id="selfieoutput" src={require('../../../../../assets/img/info.jpg')} />
    </div>
</div>
                                              {/* <div className="form-group row">
    <img id="output" width="100" height="100" />
</div> */}


                                              <div className="form-group row">
                                                  <div className="col-sm-3"><img className="img-fluid" src={require('../../../../../assets/img/info.jpg')} /></div>
                                                  <div className="col-sm-9">
                                                      <small className="form-text text-muted">Immer mit wichtigen Menschen verbunden sein und überall von einer stabilen Verbindung profitieren. Privat oder geschäftlich, Düsselstrom Interaktiv ist die einfache Lösung für mehr Flexibilität, Unabhängigkeit und Sicherheit,Immer mit wichtigen Menschen verbunden sein</small>
                                                    </div>
                                                </div>
                                            </div>
                                          <div className="pertition-box">
                                              <aside>Driver License Verification</aside>
                                              <div className="form-group row">
                                                  <label className="bmd-label-static col-sm-3">Driver License Number</label>
                                                  <input type="text" className="form-control col-sm-5" id="exampleInputEmail1" name="drivingtype" onChange={this.handleChange.bind(this)} value={this.state.drivingtype} placeholder="Enter Driving Licence Number" />
                                                </div>
                                              <div className="form-group row">
                                                  <label className="bmd-label-static col-sm-3">Driver License Front</label>
                                                  <div className="custom-file col-sm-5">
                    <input className="custom-file-input" id="validatedCustomFile" required="" type="file" onChange={this.driverLicenseChangedHandler.bind(this,0)} />
                                                      <label className="custom-file-label" >Upload photos</label>
                                                    </div>
                                                    <div className="col-sm-2 upload-image-box">
                                                      <img className="img-fluid" id="driveroutput" src={require('../../../../../assets/img/info.jpg')} />
                                                    </div>
                                                </div>
                                              {/* <div className="form-group row">
                                                  <label className="bmd-label-static col-sm-3">Driver License Back</label>
                                                  <div className="custom-file col-sm-5">
                    <input className="custom-file-input" id="validatedCustomFile" required="" type="file" onChange={this.driverLicenseChangedHandler.bind(this, 1)}/>
                                                      <label className="custom-file-label" >Upload photos</label>
                                                    </div>
                                                    <div className="col-sm-2 upload-image-box">
                                                      <img className="img-fluid" src={require('../../../../../assets/img/info.jpg')} />
                                                    </div>
                                                </div> */}
                                            </div>
                                          <div className="form-group mt-3">
                                              <button type="submit" className="btn btn-primary btn-raised px-3">Save & Verify</button>
                                              <button type="button" onClick={this.onSkipVerification.bind(this)} className="btn btn-secondary btn-raised px-3 ml-3">Skip Verification</button>
                                            </div>
                                          <div className="form-group">
                                              <small className="form-text text-muted">Note: You will need to provide verification to create campaign or become a donor</small>
                                            </div>
                                        </form>
                                    </div>
           </div>
      );
    }
}

export default VerificationForm;
