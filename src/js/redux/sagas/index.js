import { all } from 'redux-saga/effects'
import { exampleSaga } from './exampleSaga';

import { homeSaga } from './homeSaga';
import { signupSaga } from './signupSaga';
import { loginSaga } from './loginSaga';
import { editprofileSaga } from './editprofileSaga';

export default function* sagas() {
  yield all([
    ...exampleSaga,
    ...homeSaga,
    ...signupSaga,
    ...loginSaga,
    ...editprofileSaga
  ]);
}
