import React, { PureComponent } from 'react';
import Modal from 'react-modal';
import { applyNow, validateOfferCode } from '../../api/commonApi';
import { NotificationManager } from 'react-notifications';
const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',

    },
  
};


class ApplyNow extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            modalIsOpen: false,
            qrcode: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleOfferCodeSubmit = this.handleOfferCodeSubmit.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        const el = document.getElementById('app-container');
        Modal.setAppElement(el)
    }
    openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({ modalIsOpen: false });
    }
    handleChange(event) {
        var change = {};
        change[event.target.name] = event.target.value;
        this.setState(change);
    }
    handleSubmit(event) {

        if (this.state.first_name.trim() && this.state.last_name.trim() && this.state.email.trim()) {
            applyNow(this.state).then((res) => {
                NotificationManager.success("Submitted successfully.", "Success");
                this.setState({
                    first_name: '',
                    last_name: '',
                    email: ''
                })
            }).catch((err) => {
                NotificationManager.error("Please try again later", "Error");

            })
        }
        else {
            NotificationManager.error("First Name, Last Name and Email are required.", "Error");

        }

        event.preventDefault();
    }

    handleOfferCodeSubmit(event) {
        if (this.state.qrcode) {
            validateOfferCode(this.state.qrcode).then((res) => {
                if (res.data.id) {
                    this.setState({ modalIsOpen: false });
                    this.props.history.push('/application');
                }
            }).catch((err) => {
                NotificationManager.error("Offer Code not found.", "Error");
            })
        }
        else {
            NotificationManager.error("Please enter the offer code.", "Error");
        }
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <section className="inner-banner" >

                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9 col-md-9 ml-auto mr-auto">
                                <div className="home-banner-text mt-5">
                                    <h2>Coming Soon!</h2>
                                    <h6 className="mb-4">Our waiting list is now open!</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div className="inner-body my-4">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-2">
                            </div>
                            <div className="col-md-8 col-12">
                                <div className="apply-form">
                                    <button type="button" className="apply-bt mb-3" onClick={() => this.openModal()}>I have an offer code</button>
                                    <p><span>OR</span></p>
                                    <p className="mb-5">Sign Up for our waiting list</p>
                                    <form className="my-4" onSubmit={this.handleSubmit}>
                                        <div className="form-group">
                                            <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="First Name" name="first_name" onChange={this.handleChange.bind(this)} value={this.state.first_name} required />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Last Name" name="last_name" onChange={this.handleChange.bind(this)} value={this.state.last_name} required />
                                        </div>
                                        <div className="form-group">
                                            <input type="email" className="form-control" id="exampleInputPassword1" placeholder="Email" name="email" onChange={this.handleChange.bind(this)} value={this.state.email} required />
                                        </div>
                                        <button type="submit" className="apply-bt">Submit</button>
                                    </form>
                                </div>
                            </div>
                            <div className="col-md-2">
                            </div>
                        </div>
                    </div>
                </div>

                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    className="cust-modal"
                    contentLabel="Example Modal"
                    ariaHideApp={false}
                >
                    <h5  ref={subtitle => this.subtitle = subtitle}>Enter your offer code</h5>
                    
                    <a onClick={this.closeModal}><i className="fa fa-close"></i></a>
                    <form onSubmit={this.handleOfferCodeSubmit} >
                        <div className="form-group">
                            <input placeholder="Enter offer code" name="qrcode" className="form-control" onChange={this.handleChange.bind(this)} value={this.state.qrcode} required />
                        </div>

                        <button type="submit" className="apply-bt">Submit</button>
                    </form>
                </Modal>
            </div>




        )
    }
}


export default ApplyNow;
