import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import type { editprofileType } from '../../common/types/editprofile';

const GET_EDITPROFILE = 'app/editprofile/GET_EDITPROFILE';
const SET_EDITPROFILE = 'app/editprofile/SET_EDITPROFILE';
//const UPDATE_EDITPROFILE_CAMPAIGNS = 'app/editprofile/UPDATE_EDITPROFILE_CAMPAIGNS';


const GET_COUNTRIES = 'app/editprofile/GET_COUNTRIES';
const SET_COUNTRIES = 'app/editprofile/SET_COUNTRIES';

const GET_STATELIST = 'app/editprofile/GET_STATELIST';
const SET_STATELIST = 'app/editprofile/SET_STATELIST';

const GET_CITYLIST = 'app/editprofile/GET_CITYLIST';
const SET_CITYLIST = 'app/editprofile/SET_CITYLIST';



export const constants = {
  GET_EDITPROFILE,
  SET_EDITPROFILE,

  GET_COUNTRIES,
  SET_COUNTRIES,
  GET_STATELIST,
  SET_STATELIST,
  GET_CITYLIST,
  SET_CITYLIST,
};

export const getEditProfile = createAction(GET_EDITPROFILE, (userId:any) => ({userId}));
export const setEditProfile = createAction(SET_EDITPROFILE, (profile: editprofileType) => ({ profile }));
//export const updateEditProfile = createAction(UPDATE_EDITPROFILE_CAMPAIGNS, (result : editprofileType) => ({ result }));


export const getCountries = createAction(GET_COUNTRIES, () => ({}));
export const setCountries = createAction(SET_COUNTRIES, (countries: editprofileType) => ({ countries }));

export const getStateList = createAction(GET_STATELIST, (id:any) => ({id}));
export const setStateList = createAction(SET_STATELIST, (states: editprofileType) => ({ states }));

export const getCityList = createAction(GET_CITYLIST, (id:any) => ({id}));
export const setCityList = createAction(SET_CITYLIST, (cities: editprofileType) => ({ cities }));



export const actions = {
  getEditProfile,
  setEditProfile,

  getCountries,
  setCountries,
  getStateList,
  setStateList,
  getCityList,
  setCityList,
};
export const initialState = () =>
  Map({
    result: '',
    profile: '',

    countries : '',
    states : '',
    cities : ''
  })

export const reducers = {
  [SET_EDITPROFILE]: (state, { payload }) =>
    state.merge({
      ...payload,
    }),
    
    [SET_COUNTRIES]: (state, { payload }) =>
      state.merge({
        ...payload,
      }),
      [SET_STATELIST]: (state, { payload }) =>
        state.merge({
          ...payload,
        }),      
        [SET_CITYLIST]: (state, { payload }) =>
          state.merge({
            ...payload,
          }),
}

export default handleActions(reducers, initialState());
