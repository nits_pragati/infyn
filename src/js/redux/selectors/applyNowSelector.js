import {
  createSelector
} from 'reselect';

const applyNowDataSelector = state => state.home;

const resultSelector = createSelector(
  applyNowDataSelector,
  payload => payload.get('result')

);


const userDataTypeSelector = createSelector(
  applyNowDataSelector,
  payload => payload.get('resulttype')
);


export const applyNowSelector = state => ({
  result: resultSelector(state),
  resulttype: userDataTypeSelector(state),
});
