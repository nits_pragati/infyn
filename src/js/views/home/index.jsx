import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import LazyLoading from '../../common/components/LazyLoading/LazyLoading';
import { actions as homeActions } from '../../redux/modules/home';
import { homeSelector } from '../../redux/selectors/homeSelector';
import { ExampleWithError } from '../../common/components/Example';
import { ErrorBoundary } from '../../common/components/Utilities';
const LazySample = Loadable({
  loader: () => import('../../common/components/Home'),
  loading: LazyLoading,
})
const mapDispatchToProps = {
  ...homeActions,
};
const mapStateToProps = state => ({
  home: homeSelector(state),
});
@connect(mapStateToProps, mapDispatchToProps)

class HomeView extends Component {
    static propTypes = {
      //home: PropTypes.object.isRequired,
    }

    componentDidMount() {
    }
    render() {
      return (
        <Fragment>
          <LazySample {...this.props} />
          <ErrorBoundary>
          <ExampleWithError {...this.props} />
        </ErrorBoundary>
        </Fragment>
      )
    }
}

export default HomeView;
