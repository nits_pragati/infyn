import React, { PureComponent } from 'react';
import { postApplication } from '../../api/commonApi';
import { Tabs, Tab } from 'react-bootstrap-tabs';
import Collapsible from 'react-collapsible';
import InputMask from 'react-input-mask';
import { NotificationManager } from 'react-notifications';
const divStyle = {
    color: '#2f6caa',
    fontWeight: 600,
    padding: 0

};
const disabledStatus = {
    color: 'black',
    background: 'gray',
    border: '2px solid #3c8cc3',
    borderRadius: '50px',
    color: '#fff',
    fontSize: '14px',
    padding: '8px 20px',
    fontWeight: 400
}
class Application extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            loan_amount: '',
            loan_purpose: 'Car financing',
            current_employment_status: 'Self Employed',
            education: 'Less than high school',
            selectedTab: 0,
            secondDisabled: true,
            annual_income: '',
            date_of_birth: '',
            mortgage_or_rent_amount: '',
            car_loan_or_lease_amount: '',
            min_student_loans: '',
            min_credit_card_payment: '',
            personal_loan_amount: '',
            child_or_other_payment: '',
            first_name: '',
            last_name: '',
            phone: '',
            housing_status: 'Rent',
            address: '',
            citizen_status: 'US citizen',
            ssn_number: '',
            email: '',
            password: '',
            thirdDisabled: true
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.goToSecondStep = this.goToSecondStep.bind(this);
        this.goToFinalStep = this.goToFinalStep.bind(this);
        this.handleFinalSubmit = this.handleFinalSubmit.bind(this);
    }
    componentDidMount() {
        document.addEventListener("mousewheel", function (event) {
            if (document.activeElement.type === "number") {
                document.activeElement.blur();
            }
        });
    }


    handleChange(event) {
        var change = {};
        change[event.target.name] = event.target.value;
        this.setState(change);
    }
    handleSubmit(event) {
        event.preventDefault();
    }
    handleFinalSubmitError() {
        NotificationManager.error("Please fill full details to submit your applilcation.", "Error");

    }

    onSelected(index, label) {
        this.setState({ selectedTab: index });
    }

    goToSecondStep(event) {

        if (this.state.annual_income && this.state.current_employment_status && this.state.date_of_birth && this.state.education && this.state.loan_amount && this.state.loan_purpose) {
            const amount = Number(this.state.loan_amount);
            if (amount >= 1000 && amount < 50000) {
                const dob = new Date(this.state.date_of_birth);
                if (dob != "Invalid Date") {
                    const currentDate = new Date();
                    if(currentDate<dob)
                    {
                        NotificationManager.error("Future date can not be selected.", "Error");
                    }
                   else  if(this.state.annual_income==0)
                    {
                        NotificationManager.error("Please give a valid loan amount.", "Error");
                        
                    }
                    else
                    {
                        this.setState({
                            selectedTab: 1,
                            secondDisabled: false
                        })

                        if (document.getElementById("tabsElement"))
                            document.getElementById('tabsElement').scrollIntoView();
                    }
                    
                }
                else {
                    NotificationManager.error("Please give a valid date.", "Error");
                }

            }
            else {
                NotificationManager.error("Choose an amount between $1,000 and $50,000.", "Error");

            }

        }
        else {
            NotificationManager.error("All fields are required.", "Error");

        }
        event.preventDefault();
    }

    handleFinalSubmit(event) {
        if (this.state.first_name && this.state.phone && this.state.housing_status && this.state.address && this.state.citizen_status && this.state.ssn_number && this.state.email && this.state.password) {
            postApplication(this.state).then((res) => {
                this.props.history.push('business');
            }).catch((err) => {
                NotificationManager.error("Please try again later", "Error");

            })

        }
        else {
            NotificationManager.error("All fields are required.", "Error");
        }
        event.preventDefault();
    }

    goToFinalStep(event) {
       
        if (this.state.mortgage_or_rent_amount && this.state.car_loan_or_lease_amount && this.state.min_student_loans && this.state.min_credit_card_payment && this.state.personal_loan_amount && this.state.child_or_other_payment) {
            if(this.state.mortgage_or_rent_amount==0)
            {
                NotificationManager.error("Please give a valid Mortgage payment", "Error"); 
            }
            else if(this.state.car_loan_or_lease_amount==0)
            {
                NotificationManager.error("Please give a valid Lease payment", "Error"); 
            }
            else if (this.state.min_student_loans == 0) {
                NotificationManager.error("Please give a valid min student loan amount", "Error");
            }
            else if(this.state.min_credit_card_payment==0)
            {
                NotificationManager.error("Please give a valid Min Credit Card Payment", "Error"); 
            }
            else if (this.state.personal_loan_amount==0) {
                NotificationManager.error("Please give a valid Personal Loan amount", "Error");
            }
            else if (this.state.child_or_other_payment==0) {
                NotificationManager.error("Please give a valid Regular amount", "Error");
            }
            else
            {
                if (document.getElementById("tabsElement"))
                    document.getElementById('tabsElement').scrollIntoView();
                this.setState({
                    selectedTab: 2,
                    thirdDisabled: false
                })
            }
            
        }
        else {
            NotificationManager.error("All fields are required.", "Error");
        }
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <section className="inner-banner applyNow">

                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9 col-md-9 ml-auto mr-auto">
                                <div className="home-banner-text mt-5">
                                    <h2>Check Your Rate</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div className="inner-body my-4" id="tabsElement">
                    <div className="container">
                        <div className="row">

                            <div className="col-md-8">
                                <div className="app-form" >
                                    <Tabs onSelect={(index, label) => this.onSelected(index, label)} selected={this.state.selectedTab} >
                                        <Tab id="application" label="Application"><div className="tab-content">
                                            <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

                                                <form onSubmit={this.goToSecondStep}>
                                                    <div className="line">
                                                        <h5>How much would you like to borrow?</h5>
                                                        <p>Choose an amount between $1,000 and $50,000.</p>
                                                        <div className="form-group row">
                                                            <label className="col-md-6 col-lg-4">Loan Ammount</label>
                                                            <div className="col-md-6 col-lg-8">
                                                                <input type="number" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="$" name="loan_amount" onChange={this.handleChange.bind(this)} value={this.state.loan_amount} required />
                                                            </div>
                                                        </div>
                                                        <div className="form-group row">
                                                            <label className="col-md-6 col-lg-4">Loan Purpose</label>
                                                            <div className="col-md-6 col-lg-8">
                                                                <select className="form-control" id="exampleFormControlSelect1" name="loan_purpose" onChange={this.handleChange.bind(this)} value={this.state.loan_purpose} required>
                                                                    <option value="Credit card refinancing">Credit card refinancing</option>
                                                                    <option value="Debt consolidation">Debt consolidation</option>
                                                                    <option value="Home improvement">Home improvement</option>
                                                                    <option value="Major purchase">Major purchase</option>
                                                                    <option value="Home buying">Home buying</option>
                                                                    <option value="Car financing">Car financing</option>
                                                                    <option value="Green loan">Green loan</option>
                                                                    <option value="Business">Business</option>
                                                                    <option value="Vacation">Vacation</option>
                                                                    <option value="Moving/relocation">Moving/relocation</option>
                                                                    <option value="Medical expenses">Medical expenses</option>
                                                                    <option value="Wedding">Wedding</option>
                                                                    <option value="Motorcycle">Motorcycle</option>
                                                                    <option value="RV">RV</option>
                                                                    <option value="Boat">Boat</option>
                                                                    <option value="Taxes">Taxes</option>
                                                                    <option value="Baby or adoption">Baby or adoption</option>
                                                                    <option value="Engagement ring financing ">Engagement ring financing </option>
                                                                    <option value="Cosmetic procedures">Cosmetic procedures</option>
                                                                    <option value="Special occasion">Special occasion</option>
                                                                    <option value="Other">Other</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="line">
                                                        <div className="form-group row">
                                                            <label className="col-md-6 col-lg-4">Current Employment Type</label>
                                                            <div className="col-md-6 col-lg-8">
                                                                <select className="form-control" id="exampleFormControlSelect2" name="current_employment_status" onChange={this.handleChange.bind(this)} value={this.state.current_employment_status} required>
                                                                    <option value="Self Employed">Self Employed</option>
                                                                    <option value="Unemployed/Not employed">Unemployed/Not employed</option>
                                                                    <option value="Employed - Full Time">Employed - Full Time</option>
                                                                    <option value="Employed - Part Time">Employed - Part Time</option>
                                                                    <option value="Military">Military</option>
                                                                    <option value="Retired benefits">Retired benefits</option>
                                                                    <option value="Other">Other</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="line">
                                                        <div className="form-group row">
                                                            <label className="col-md-6 col-lg-4">Date of birth</label>
                                                            <div className="col-md-6 col-lg-8">
                                                                {/* <input type="text" className="form-control" id="exampleInputEmail22" aria-describedby="emailHelp" placeholder="$" name="date_of_birth" onChange={this.handleChange.bind(this)} value={this.state.date_of_birth} required /> */}
                                                                <InputMask placeholder="mm/dd/yyyy" mask="99/99/9999" maskChar={null} value={this.state.date_of_birth} onChange={this.handleChange.bind(this)} name="date_of_birth" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="line">
                                                        <h5>Highest Level of Education</h5>
                                                        <p>Include degrees in which you are currently enrolled.</p>
                                                        <div className="form-group row">
                                                            <label className="col-md-6 col-lg-4">Degree program</label>
                                                            <div className="col-md-6 col-lg-8">
                                                                <select className="form-control" id="exampleFormControlSelect3" name="education" onChange={this.handleChange.bind(this)} value={this.state.education} required>
                                                                    <option value="Less than high school">Less than high school</option>
                                                                    <option value="High school">High school</option>
                                                                    <option value="Certificate">Certificate</option>
                                                                    <option value="Associate">Associate</option>
                                                                    <option value="Bachelor's">Bachelor's</option>
                                                                    <option value="MBA">MBA</option>
                                                                    <option value="Master's (excluding MBA)">Master's (excluding MBA)</option>
                                                                    <option value="Dentistry (DDS/DMD)">Dentistry (DDS/DMD)</option>
                                                                    <option value="Law (JD/JSD)">Law (JD/JSD)</option>
                                                                    <option value="Medicine (MD/DO)">Medicine (MD/DO)</option>
                                                                    <option value="Pharmacy (PharmD)">Pharmacy (PharmD)</option>
                                                                    <option value="Veterinary medicine (DVM/MVD)">Veterinary medicine (DVM/MVD)</option>
                                                                    <option value="Phd">Phd</option>
                                                                    <option value="Other graduate degree">Other graduate degree</option>
                                                                    <option value="Other">Other</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="line">
                                                        <h5>How much do you earn?</h5>
                                                        <div className="form-group row">
                                                            <label className="col-md-6 col-lg-4">Annual Income</label>
                                                            <div className="col-md-6 col-lg-8">
                                                                <input type="number" className="form-control" id="exampleInputEmail19" aria-describedby="emailHelp" placeholder="$" name="annual_income" onChange={this.handleChange.bind(this)} value={this.state.annual_income} required />
                                                            </div>
                                                        </div>
                                                        {/* <div className="form-group row">
                                                            <label className="col-md-6 col-lg-4">Investment accounts:</label>
                                                            <div className="col-md-6 col-lg-8">
                                                                <select className="form-control" id="exampleFormControlSelect5" name="investment_account" onChange={this.handleChange.bind(this)} value={this.state.investment_account}>
                                                                    <option>$</option>
                                                                    <option>$</option>
                                                                </select>
                                                            </div>
                                                        </div> */}
                                                    </div>

                                                    <div className="line">
                                                        {/* <h5>How much do you have in savings?</h5>
                                                        <div className="form-group">
                                                            <div className="form-check">
                                                                <input className="form-check-input" type="checkbox" value="" id="defaultCheck1" />
                                                                <label className="form-check-label" >
                                                                    Yes
														</label>
                                                            </div>
                                                        </div>
                                                        <div className="form-group">
                                                            <div className="form-check">
                                                                <input className="form-check-input" type="checkbox" value="" id="defaultCheck2" />
                                                                <label className="form-check-label" >
                                                                    No
														</label>
                                                            </div>
                                                        </div> */}
                                                        <div className="form-group">
                                                            <button type="submit" className="btn-primery"  >Next Step</button>
                                                        </div>

                                                    </div>
                                                </form>

                                            </div>
                                        </div></Tab>
                                        <Tab label="Debt to Income">
                                            <form onSubmit={this.goToFinalStep}>
                                                <div className="line">
                                                    <div className="form-group mb-4 row">
                                                        <label className="col-md-6 col-lg-4">Mortgage or rent payment</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            {/* <select className="form-control" id="exampleFormControlSelect6">
                                                                <option>$</option>
                                                                <option>$</option>
                                                            </select> */}
                                                            <input type="number" className="form-control" aria-describedby="emailHelp" placeholder="$" name="mortgage_or_rent_amount" onChange={this.handleChange.bind(this)} value={this.state.mortgage_or_rent_amount} required />
                                                        </div>
                                                    </div>
                                                    <div className="form-group mb-4 row">
                                                        <label className="col-md-6 col-lg-4">Car loan or lease payments</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            {/* <select className="form-control" id="exampleFormControlSelect7">
                                                                <option>$</option>
                                                                <option>$</option>
                                                            </select> */}
                                                            <input type="number" className="form-control" aria-describedby="emailHelp" placeholder="$" name="car_loan_or_lease_amount" onChange={this.handleChange.bind(this)} value={this.state.car_loan_or_lease_amount} required />
                                                        </div>
                                                    </div>
                                                    <div className="form-group mb-4 row">
                                                        <label className="col-md-6 col-lg-4">Minimum student loans payment</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            {/* <select className="form-control" id="exampleFormControlSelect8">
                                                                <option>$</option>
                                                                <option>$</option>
                                                            </select> */}
                                                            <input type="number" className="form-control" aria-describedby="emailHelp" placeholder="$" name="min_student_loans" onChange={this.handleChange.bind(this)} value={this.state.min_student_loans} required />
                                                        </div>
                                                    </div>
                                                    <div className="form-group mb-4 row">
                                                        <label className="col-md-6 col-lg-4">Minimum credit cards payment</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            {/* <select className="form-control" id="exampleFormControlSelect9">
                                                                <option>$</option>
                                                                <option>$</option>
                                                            </select> */}
                                                            <input type="number" className="form-control" aria-describedby="emailHelp" placeholder="$" name="min_credit_card_payment" onChange={this.handleChange.bind(this)} value={this.state.min_credit_card_payment} required />
                                                        </div>
                                                    </div>
                                                    <div className="form-group mb-4 row">
                                                        <label className="col-md-6 col-lg-4">Personal loan payment</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            {/* <select className="form-control" id="exampleFormControlSelect10">
                                                                <option>$</option>
                                                                <option>$</option>
                                                            </select> */}
                                                            <input type="number" className="form-control" aria-describedby="emailHelp" placeholder="$" name="personal_loan_amount" onChange={this.handleChange.bind(this)} value={this.state.personal_loan_amount} required />
                                                        </div>
                                                    </div>
                                                    <div className="form-group mb-4 row">
                                                        <label className="col-md-6 col-lg-4">Child Support & other regular payments</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            {/* <select className="form-control" id="exampleFormControlSelect11">
                                                                <option>$</option>
                                                                <option>$</option>
                                                            </select> */}
                                                            <input type="number" className="form-control" aria-describedby="emailHelp" placeholder="$" name="child_or_other_payment" onChange={this.handleChange.bind(this)} value={this.state.child_or_other_payment} required />

                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    {
                                                        this.state.secondDisabled ? (
                                                            <button type="button" style={disabledStatus} onClick={() => this.handleFinalSubmitError()}>Next Step</button>
                                                        ) : (
                                                                <button type="submit" className="btn-primery" >Next Step</button>
                                                            )
                                                    }

                                                </div>
                                            </form>
                                        </Tab>
                                        <Tab label="Profile">
                                            <form onSubmit={this.handleFinalSubmit}>
                                                <div className="line">
                                                    <h5>Please provide a few details about yourself</h5>
                                                    <div className="form-group row">
                                                        <label className="col-md-6 col-lg-4">First Name</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            <input type="text" className="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" placeholder="First Name" name="first_name" onChange={this.handleChange.bind(this)} value={this.state.first_name} required />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label className="col-md-6 col-lg-4">Last Name</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            <input type="text" className="form-control" id="exampleInputEmail3" aria-describedby="emailHelp" placeholder="Last Name" name="last_name" onChange={this.handleChange.bind(this)} value={this.state.last_name} required />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label className="col-md-6 col-lg-4">Primary Phone Number</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            <input type="number" className="form-control" id="exampleInputEmail4" aria-describedby="emailHelp" placeholder="Phone" name="phone" onChange={this.handleChange.bind(this)} value={this.state.phone} required />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label className="col-md-6 col-lg-4">Housing Status</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            <select className="form-control" id="exampleFormControlSelect12" name="housing_status" onChange={this.handleChange.bind(this)} value={this.state.housing_status} required>
                                                                <option value="Rent">Rent</option>
                                                                <option value="Own - with mortgage">Own - with mortgage</option>
                                                                <option value="Own - without mortgage">Own - without mortgage</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label className="col-md-6 col-lg-4">Permanent Address</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            <input type="text" className="form-control" id="exampleInputEmail5" aria-describedby="emailHelp" placeholder="PO Box not allowed" name="address" onChange={this.handleChange.bind(this)} value={this.state.address} required />
                                                        </div>
                                                    </div>
                                                    {/* <div className="form-group row">
                                                        <label className="col-md-6 col-lg-4">Unit(ootional)</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            <input type="text" className="form-control" id="exampleInputEmail6" aria-describedby="emailHelp" placeholder="" name="unit" onChange={this.handleChange.bind(this)} value={this.state.unit} required/>
                                                        </div>
                                                    </div> */}
                                                    <div className="form-group row">
                                                        <label className="col-md-6 col-lg-4">Citizenship Status</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            <select className="form-control" id="exampleFormControlSelect13" name="citizen_status" onChange={this.handleChange.bind(this)} value={this.state.citizen_status} required>
                                                                <option value="US citizen">US citizen</option>
                                                                <option value="Permanent Resident Alien">Permanent Resident Alien</option>
                                                                <option value="Non-resident Alien">Non-resident Alien</option>
                                                                <option value="H1 Visa Holder">H1 Visa Holder</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label className="col-md-6 col-lg-4">Social security number</label>
                                                        <div className="col-md-6 col-lg-8">
                                                            <div className="input-group">
                                                                <input type="password" className="form-control" placeholder="" aria-label="Recipient's username" aria-describedby="basic-addon9" name="ssn_number" onChange={this.handleChange.bind(this)} value={this.state.ssn_number} required />
                                                                <div className="input-group-append">
                                                                    <span className="input-group-text" id="basic-addon2"><i className="fa fa-lock"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  
                                                  
                                                    <div className="login-box">
                                                        <div className="form-group row">
                                                            <label className="col-md-6 col-lg-4">Email Address</label>
                                                            <div className="col-md-6 col-lg-8">
                                                                <input type="email" className="form-control" id="exampleInputEmail7" aria-describedby="emailHelp" placeholder="" name="email" onChange={this.handleChange.bind(this)} value={this.state.email} required/>
                                                            </div>
                                                        </div>
                                                        <div className="form-group row">
                                                            <label className="col-md-6 col-lg-4">Password</label>
                                                            <div className="col-md-6 col-lg-8">
                                                                <input type="password" className="form-control" id="exampleInputEmail8" aria-describedby="emailHelp" placeholder="" name="password" onChange={this.handleChange.bind(this)} value={this.state.password} required />
                                                            </div>
                                                        </div>
                                                      

                                                    </div>
                                                    <div className="mt-3">
                                                        <h5>Terms & Conditions</h5>
                                                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                                                    </div>
                                                    <div className="form-group">
                                                        {
                                                            this.state.thirdDisabled ? (
                                                                <button type="button" style={disabledStatus} onClick={() => this.handleFinalSubmitError()}>Agree and find my rates</button>
                                                            ) : (
                                                                    <button type="submit" className="btn-primery" >Agree and find my rates</button>
                                                                )
                                                        }

                                                    </div>
                                                </div>

                                            </form>
                                        </Tab>
                                    </Tabs>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="app-right">
                                    <h3>Common Questions</h3>
                                    <div className="app-collapse">
                                        <div className="card">
                                            <Collapsible trigger="Collapsible Group Item #1" openedClassName="accordionHeader" classParentString="myAccordionClass" triggerOpenedClassName="triggerOpenedClass" triggerStyle={divStyle} contentOuterClassName="outerClass">
                                                <div className="card-body">
                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat haven't heard of them accusamus labore sustainable VHS.
                                          </div>
                                            </Collapsible>
                                        </div>

                                        <Collapsible trigger="Collapsible Group Item #2" openedClassName="accordionHeader" classParentString="myAccordionClass" triggerOpenedClassName="triggerOpenedClass" triggerStyle={divStyle} contentOuterClassName="outerClass">
                                            <div className="card-body">
                                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat haven't heard of them accusamus labore sustainable VHS.
                                          </div>
                                        </Collapsible>
                                        <Collapsible trigger="Collapsible Group Item #3" openedClassName="accordionHeader" classParentString="myAccordionClass" triggerOpenedClassName="triggerOpenedClass" triggerStyle={divStyle} contentOuterClassName="outerClass">
                                            <div className="card-body">
                                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat haven't heard of them accusamus labore sustainable VHS.
                                          </div>
                                        </Collapsible>
                                        {/* <div className="middle">
                                            <button type="button" className="btn btn-outline-dark rounded-btn col-btn">View More</button>
                                        </div> */}
                                    </div>
                                    <div className="calling row my-4">
                                        <div className="col-lg-4 img img-fluid">
                                            <img src={require('../../../../assets/img/img.jpg')} alt="" />
                                        </div>
                                        <div className="col-lg-8 ">
                                            <h5>Need Help?</h5>
                                            {/* <button type="button" className="btn-primery">Chat Now</button> */}
                                            <div className="mt-3">
                                                <p className="mb-1"><i className="fa fa-envelope pr-2"></i>support@email.com</p>
                                                <p className="mb-1"><i className="fa fa-phone pr-2"></i>866-540-6005</p>
                                                <p className="pl-3 mb-1">Mon-Thu:6am-6pm PT</p>
                                                <p className="pl-3 mb-1">Fri:6am-4pm PT</p>
                                                <p className="pl-3 mb-1">Sat-Sun:7am-4pm</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



        )
    }
}


export default Application;
