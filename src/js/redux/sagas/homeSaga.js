import { put, fork, takeLatest, call } from 'redux-saga/effects';
import { constants as homeConstants, actions as homeActions } from '../modules/home';

import type { homeType } from '../../common/types/home';

export function* fetchHomeData(actions) {
  // pretend there is an api call
  var id,page,size;
  size = actions.payload.size?actions.payload.size:50;
  id = actions.payload.id?actions.payload.id:0;
  page = actions.payload.page?actions.payload.page:0;


  console.log('payload --- ',id);
  const response = yield call(() => fetch(__CONFIG__.api_url+'home?topicId=' + id + '&page=' + page + '&size=' + size,
  {method:'GET',data:{},headers: {
    "Accept-Language":"en-US",
    "Content-Type": "application/json"
  }}).then(response => response.json()))

  console.log('bikash', response);
  yield put(homeActions.updateHomeCampaigns(response.data));

}


export function* fetchHomeTypeData() {

  //Second Changes by Suman

  const responsetype = yield call(() => fetch(__CONFIG__.api_url+'topic/0',{method:'GET',data:{}}).then(response => response.json()))

  console.log('SUMANNNN BY CATEGORY', responsetype);
  yield put(homeActions.updateHomeTypeCampaigns(responsetype.data));

  //Second Changes by Suman End

}



function* watchGetHome() {
  yield takeLatest(homeConstants.GET_HOME_CAMPAIGNS, fetchHomeData);
  yield takeLatest(homeConstants.GET_HOMETYPE_CAMPAIGNS, fetchHomeTypeData);
}


export const homeSaga = [
  fork(watchGetHome),
];
