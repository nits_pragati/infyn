import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import Sidebar from '../Sidebar';
import { connect } from 'react-redux';
import { actions as loginActions } from '../../../redux/modules/login';
import { loginSelector } from '../../../redux/selectors/loginSelector';
import { getBusinessList } from '../../api/commonApi';
class Contact extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            businessList: []
        }

    }

    componentDidMount() {
        getBusinessList().then((res) => {
            if (res.data) {
                this.setState({ businessList: res.data.results });
            }
        }).catch((err) => {

        })


    }


    // render() {
    //   const { editprofile } = this.props;
    //   let profileDetails;
    //   if (editprofile && editprofile.profile && !this.state.initialized) {
    //     profileDetails = editprofile.profile.toJS();
    //    profileDetails = profileDetails.data;
    //  }
    //     return (
    //         <div>
    //           <section className="py-4">
    //                 <div className="container">
    // 		            <div className="row">
    //                         <Sidebar />
    //                         {profileDetails?(
    //                         <div className="col-md-9">
    //                             <div className="card dashboard-panel">
    //                                 <div className="card-header">Dashboard</div>
    //                                 <div className="card-body my-dahsboard">
    //                                   <div className="media">
    //                                       <img className="align-self-center mr-3" src={profileDetails.profileImg?__CONFIG__.user_image + profileDetails.profileImg:require('../../../../assets/img/profile.png')} alt="Generic placeholder image" />
    //                                       <div className="media-body align-self-center">
    //                                           <h6 className="mt-0">{profileDetails.firstname} {profileDetails.lastname}</h6>
    //                                           <p>{profileDetails.email} <br/>{profileDetails.phoneNumber}</p>
    //                                       </div>
    //                                       <div className="media-right align-self-center">
    //                                       <Link to="/editprofile">  <button type="button" className="btn btn-light"><i className="fa fa-edit"></i> Edit</button> </Link>
    //                                       </div>
    //                                   </div>
    //                                     <h5 className="my-4">My Latest Campaigns</h5>
    //                                     <div className="latest-campaigns">
    //                                         {this.state.myCampaigns?this.state.myCampaigns.map((campaign,key)=>(
    //                                           <div key={key} className="media">
    //                                             {campaign.images?(
    //                                               <img className="align-self-center mr-3" src={campaign.images?__CONFIG__.campaign_image + campaign.images['0']:require('../../../../assets/img/details.jpg')} alt="Generic placeholder image" />

    //                                             ):''}

    //                                               <div className="media-body align-self-center">
    //                                                   <div className="d-flex justify-content-between">
    //                                                       <div>
    //                                                           <h6 className="mt-0">{campaign.campaignName}</h6>
    //                                                           <p>by {campaign.user.firstname} {campaign.user.lastname} <br/>Campaign ID: <b>{campaign.id}</b> </p>
    //                                                       </div>
    //                                                       <div>
    //                                                           <button type="button"><i className="fa fa-edit"></i></button>
    //                                                           <button type="button"><i className="fa fa-close"></i></button>
    //                                                       </div>
    //                                                   </div>
    //                                                   <div className="progress">
    //                                                       <div className="progress-bar"></div>
    //                                                   </div>
    //                                                   <div className="d-flex justify-content-between">
    //                                                       <p>${campaign.goalAmount?campaign.goalAmount:0.00} USD</p>
    //                                                       <p>82% funded</p>
    //                                                       <p>16 days left</p>
    //                                                   </div>
    //                                               </div>
    //                                           </div>
    //                                         )):''}
    //                                     </div>
    //                                 </div>
    //                             </div>
    //                         </div>):''}
    //                     </div>
    //                 </div>
    //             </section>
    //         </div>
    //        );
    // }


    render() {
        return (
            <div>
                <section className="nav-padd"></section>
                <section className="page-nav">
                    <div className="container">
                        <ul className="list-inline">
                            <li> <Link to="/">Home</Link></li>
                            <li>Category</li>
                        </ul>
                    </div>
                </section>
                <section className="list-body">
                    <div className="container">
                        {
                            this.state.businessList && this.state.businessList.length && this.state.businessList.length > 0 ?

                                this.state.businessList.map((item, key) => {
                                    return (
                                        <div className="list-container" key={key}>
                                            <div className="outer-list">
                                                <div className="row">
                                                    <div className="col-12 col-lg-9">
                                                        <div className="left-div">
                                                            <h2>{item.name}</h2>
                                                            <div className="divide-list clearfix">
                                                                <ul className="list-inline clearfix">
                                                                    <li>
                                                                        <div className="l-box">
                                                                            <figure className="py-2">
                                                                                <img src={item.image} className="img-fluid" alt="LOGO" />
                                                                            </figure>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div className="l-box">
                                                                            <p className="mb-0">
                                                                                <span>Terms of Loan</span>
                                                                                <strong>{item.term_of_loan}</strong>
                                                                                <small>months</small>
                                                                            </p>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div className="l-box">
                                                                            <p className="mb-0">
                                                                                <span>APR Range</span>
                                                                                <strong>5.99% To 35.99%</strong>
                                                                            </p>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div className="l-box">
                                                                            <p className="mb-0">
                                                                                <span>Origination Fee</span>
                                                                                <strong>{item.organization_fee}</strong>
                                                                            </p>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-12 col-lg-3">
                                                        <div className="right-btn-div  text-center">
                                                            {/* <a href="javascript:void(0)" className="btn btn-primary">CHECK RATE</a> */}
                                                            <a href="javascript:void(0)" className="btn btn-link"><small>Read Full Review</small></a>
                                                            <a href={item.comapny_url} target="_blank" className="btn btn-danger">Hard credit pull</a>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })

                                : <div><h2>No records found.</h2></div>
                        }




                    </div>
                </section>

            </div>
        )
    }
}
const mapDispatchToProps = {
    ...loginActions,
};
const mapStateToProps = state => ({
    login: loginSelector(state),
});
export default connect(mapStateToProps, mapDispatchToProps)(Contact);
