import { createSelector } from 'reselect';

const loginDataSelector = state => state.login;

const resultSelector = createSelector(
  loginDataSelector,
  payload => payload.get('result')
);

const loginErrorSelector = createSelector(
  loginDataSelector,
  payload => payload.get('login_error')
);

const userDataSelector = createSelector(
  loginDataSelector,
  payload => payload.get('userData')
);

export const loginSelector = state => ({
  result: resultSelector(state),
  login_error:loginErrorSelector(state),
  userData:userDataSelector(state)
});
