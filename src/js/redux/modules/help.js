import {
  createAction,
  handleActions
} from 'redux-actions';
import {
  Map
} from 'immutable';
import type {
  helpType
} from '../../common/types/help';

const GET_HOME_CAMPAIGNS = 'app/home/GET_HOME_CAMPAIGNS';
const UPDATE_HOME_CAMPAIGNS = 'app/home/UPDATE_HOME_CAMPAIGNS';

const GET_HOMETYPE_CAMPAIGNS = 'app/home/GET_HOMETYPE_CAMPAIGNS';
const UPDATE_HOMETYPE_CAMPAIGNS = 'app/home/UPDATE_HOMETYPE_CAMPAIGNS';

export const constants = {
  GET_HOME_CAMPAIGNS,
  UPDATE_HOME_CAMPAIGNS,

  GET_HOMETYPE_CAMPAIGNS,
  UPDATE_HOMETYPE_CAMPAIGNS,
};

export const getHomeCampaigns = createAction(GET_HOME_CAMPAIGNS, (id: helpType, page: any) => ({
  id,
  page
}));
export const updateHomeCampaigns = createAction(UPDATE_HOME_CAMPAIGNS, (result: homeType) => ({
  result
}));


export const getHomeTypeCampaigns = createAction(GET_HOMETYPE_CAMPAIGNS, () => ({}));
export const updateHomeTypeCampaigns = createAction(UPDATE_HOMETYPE_CAMPAIGNS, (resulttype: homeType) => ({
  resulttype
}));


export const actions = {
  getHomeCampaigns,
  updateHomeCampaigns,

  getHomeTypeCampaigns,
  updateHomeTypeCampaigns,
};
export const initialState = () =>
  Map({
    result: '',
    resulttype: '',
  })

export const reducers = {
  [UPDATE_HOME_CAMPAIGNS]: (state, {
      payload
    }) =>
    state.merge({
      ...payload,
    }),

  [UPDATE_HOMETYPE_CAMPAIGNS]: (state, {
      payload
    }) =>
    state.merge({
      ...payload,
    }),
  [GET_HOME_CAMPAIGNS]: (state, {
      payload
    }) =>
    state.merge({
      ...payload,
    }),
}

export default handleActions(reducers, initialState());
