import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import LazyLoading from '../../common/components/LazyLoading/LazyLoading';
import { actions as signupActions } from '../../redux/modules/signup';
import { signupSelector } from '../../redux/selectors/signupSelector';
import { countrySelector } from '../../redux/selectors/countrySelector';
import { ExampleWithError } from '../../common/components/Example';
import { ErrorBoundary } from '../../common/components/Utilities';
const LazySample = Loadable({
  loader: () => import('../../common/components/Signup'),
  loading: LazyLoading,
})
const mapDispatchToProps = {
  ...signupActions,
};
const mapStateToProps = state => ({
  signup: signupSelector(state)
});
@connect(mapStateToProps, mapDispatchToProps)

class SignupView extends Component {

    static propTypes = {
      getSignup: PropTypes.func,
      getCountries: PropTypes.func
    }

    componentDidMount() {
      console.log('get1');
      this.props.getCountries();
      this.props.getSignup();
    }
    render() {
      return (
        <Fragment>
          <LazySample {...this.props} />
        </Fragment>
      )
    }
}

export default SignupView;
