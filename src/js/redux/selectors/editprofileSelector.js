import { createSelector } from 'reselect';

const editprofileDataSelector = state => state.editprofile;



const countrySelector = createSelector(
  editprofileDataSelector,
  payload => payload.get('countries'),

);

const citySelector = createSelector(
  editprofileDataSelector,
  payload => payload.get('cities'),

);

const stateSelector = createSelector(
  editprofileDataSelector,
  payload => payload.get('states'),

);



const resultSelector = createSelector(
  editprofileDataSelector,
  payload => payload.get('result')
);

const profileSelector = createSelector(
  editprofileDataSelector,
  payload => payload.get('profile')
);

export const editprofileSelector = state => ({
  countries: countrySelector(state),
  cities:citySelector(state),
  states:stateSelector(state),

  result: resultSelector(state),
  profile : profileSelector(state)
});
