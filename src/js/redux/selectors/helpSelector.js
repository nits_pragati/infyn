import {
  createSelector
} from 'reselect';

const helpDataSelector = state => state.home;

const resultSelector = createSelector(
  helpDataSelector,
  payload => payload.get('result')

);


const userDataTypeSelector = createSelector(
  helpDataSelector,
  payload => payload.get('resulttype')
);


export const helpSelector = state => ({
  result: resultSelector(state),
  resulttype: userDataTypeSelector(state),
});
