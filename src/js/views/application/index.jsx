import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import LazyLoading from '../../common/components/LazyLoading/LazyLoading';
import { actions as applyActions } from '../../redux/modules/apply-now';
import { applyNowSelector } from '../../redux/selectors/applyNowSelector';
import { ExampleWithError } from '../../common/components/Example';
import { ErrorBoundary } from '../../common/components/Utilities';
const LazySample = Loadable({
    loader: () => import('../../common/components/application'),
    loading: LazyLoading,
})
const mapDispatchToProps = {
    ...applyActions,
};
const mapStateToProps = state => ({
    home: applyNowSelector(state),
});
@connect(mapStateToProps, mapDispatchToProps)

class ApplicationView extends Component {
    static propTypes = {
        //home: PropTypes.object.isRequired,
    }

    componentDidMount() {
    }
    render() {
        return (
            <Fragment>
                <LazySample {...this.props} />
                <ErrorBoundary>
                    <ExampleWithError {...this.props} />
                </ErrorBoundary>
            </Fragment>
        )
    }
}

export default ApplicationView;
