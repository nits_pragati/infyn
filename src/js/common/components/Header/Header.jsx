import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { actions as loginActions } from '../../../redux/modules/login';
import { loginSelector } from '../../../redux/selectors/loginSelector';
import './Header.css';

class Header extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      campaignMenu: false,
      userMenu: false,
      IsHome: true
    }
    this.props.isLoggedIn()
    this.goToLogin = this.goToLogin.bind(this);
    this.props.history.listen((location, action) => {
      if (location && location.pathname == "/") {
        this.setState({ IsHome: true });
      }
      else {
        this.setState({ IsHome: false });
      }
    });
  }
  toggleMenu() {
    this.setState({ campaignMenu: !this.state.campaignMenu })
  }


  componentDidMount() {
    window.addEventListener('scroll', function () {
      const sticky = document.getElementsByClassName('navbar-dark');
      const scroll = window.scrollY;
      if (scroll) {
        if (sticky) {
          if (scroll >= 200) {

            sticky[0].classList.add('opacity')
          }
          else sticky[0].classList.remove('opacity');
        }
      }

    })

  }
  goToLogin() {
    //this.props.history.push('/login');
    //this.props.history.go('/login');
  }
  toggleUserMenu() {
    this.setState({ userMenu: !this.state.userMenu });
  }
  render() {
    const { pathname } = this.props.location;
    let userData;
    if (this.props.login.userData) {
      console.log('js ', this.props.login.userData.toJS())
      userData = this.props.login.userData.toJS();

    }
    console.log('header data ', this.props.login.userData)
    // const isHome = pathname === '/';
    // const isJustAnotherPage = pathname === '/page';

    return (
      <section>
        <nav className="navbar navbar-expand-lg navbar-dark fixed-header">
          <div className="container">
            <Link className="navbar-brand" to="/"><img src={require('../../../../assets/img/logo-2.png')} alt="" /></Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown1" aria-controls="navbarNavDropdown1" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavDropdown1">
              <ul className="navbar-nav ml-auto" id="test">
                <li className="nav-item active">
                  <Link to="/about" className="nav-link">About Us</Link>
                </li>
                <li className="nav-item">
                  <Link to="/help" className="nav-link">Help</Link>
                </li>
                <li className="nav-item">
                  <Link to="/" className="nav-link">Resources</Link>
                </li>
                <li>
                  <button type="button" onClick={this.goToLogin} className="btn btn-outline-dark text-uppercase rounded-btn">Sign In</button>
                </li>

              </ul>
            </div>
          </div>
        </nav>
        {
          this.state.IsHome ? (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
              <div className="container">
                <Link className="navbar-brand" to="/"><img src={require('../../../../assets/img/logo.png')} alt="" /></Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon" />
                </button>

                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                  <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                      <Link to="/about" className="nav-link">About Us</Link>
                    </li>
                    <li className="nav-item">
                      <Link to="/help" className="nav-link">Help</Link>
                    </li>
                    <li className="nav-item">
                      <Link to="/" className="nav-link">Resources</Link>
                    </li>
                    <li>

                      <button onClick={this.goToLogin} type="button" className="btn btn-outline-light text-uppercase rounded-btn">Sign In</button>
                    </li>
                  </ul>

                </div>
              </div>
            </nav>
          ) : (
              <div className="inner-nav">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                  <div className="container">
                    <Link className="navbar-brand" to="/"><img src={require('../../../../assets/img/logo.png')} alt="" /></Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                      <span className="navbar-toggler-icon" />
                    </button>

                    <div className="collapse navbar-collapse" id="navbarNavDropdown">
                      <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                          <Link to="/about" className="nav-link">About Us</Link>
                        </li>
                        <li className="nav-item">
                          <Link to="/help" className="nav-link">Help</Link>
                        </li>
                        <li className="nav-item">
                          <Link to="/" className="nav-link">Resources</Link>
                        </li>
                        <li>

                          <button onClick={this.goToLogin} type="button" className="btn btn-outline-light text-uppercase rounded-btn">Sign In</button>
                        </li>
                      </ul>

                    </div>
                  </div>
                </nav>
              </div>
            )
        }

      </section>

    );
  }
}
const mapDispatchToProps = {
  ...loginActions,
};
const mapStateToProps = state => ({
  login: loginSelector(state),
});
export default connect(mapStateToProps, mapDispatchToProps)(Header);
