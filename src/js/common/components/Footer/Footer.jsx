import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import './Footer.css';
// import Background from ;
// import Background from '../../../../assets/img/footer.jpg';


class Footer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      
    }
    
    this.goToApplyNow = this.goToApplyNow.bind(this);
  }
  goToApplyNow() {
    this.props.history.push('/applynow');
  }
  render() {
    const { pathname } = this.props.location;
    const sectionStyle = {
      backgroundImage: "url(" + require('../../../../assets/img/footer.jpg') + ")",
    };
    // const isHome = pathname === '/';
    // const isJustAnotherPage = pathname === '/page';

   
    return (
      <div>
        <section className="apply">
          <div className="container">
            <div className="row">
              <div className="col-lg-12 col-md-12 text-center">
                <h1 className="text-medium mb-5 text-white">Check Your Rate Now</h1>
                <button type="button" onClick={this.goToApplyNow} className="btn btn-outline-light text-uppercase rounded-btn btn-lg">APPLY NOW!</button>
              </div>
            </div>
          </div>
        </section>
        <footer>
          <div className="container">
            <div className="row">
              <div className="col-lg-6 col-md-6">
                <ul className="footer-links mt-2">
                  <li><Link to="/">Home</Link></li>
                  <li><Link to="/about">About Us</Link></li>
                  {/* <li><Link to="/">How it Works</Link></li>
                  <li><Link to="/">Blog</Link></li> */}
                  <li><Link to="/help">Help</Link></li>
                </ul>
              </div>
              <div className="col-lg-6 col-md-6">
                {/* <ul className="social-links">
                  <li><Link to="/"><i className="ion-social-facebook"></i></Link></li>
                  <li><Link to="/"><i className="ion-social-twitter"></i></Link></li>
                  <li><Link to="/"><i className="ion-social-youtube"></i></Link></li>
                  <li><Link to="/"><i className="ion-social-instagram"></i></Link></li>
                  <li><Link to="/"><i className="ion-social-pinterest"></i></Link></li>
                </ul> */}
              </div>
            </div>
          </div>
        </footer>
      </div>

    );
  }
}

export default Footer;
