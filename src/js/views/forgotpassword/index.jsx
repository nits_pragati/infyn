import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import LazyLoading from '../../common/components/LazyLoading/LazyLoading';
import { actions as forgotpasswordActions } from '../../redux/modules/forgotpassword';
import { forgotpasswordSelector } from '../../redux/selectors/forgotpasswordSelector';
import { ExampleWithError } from '../../common/components/Example';
import { ErrorBoundary } from '../../common/components/Utilities';
const LazySample = Loadable({
  loader: () => import('../../common/components/ForgotPassword'),
  loading: LazyLoading,
})
const mapDispatchToProps = {
  ...forgotpasswordActions,
};
const mapStateToProps = state => ({
  forgotpassword: forgotpasswordSelector(state),
});
@connect(mapStateToProps, mapDispatchToProps)

class ForgotPasswordView extends Component {
    static propTypes = {
      //home: PropTypes.object.isRequired,
    }

    // componentDidMount() {
    //   this.props.getaddcampaignCampaigns();
    // }
    render() {
      return (
        <Fragment>
          <LazySample {...this.props} />
          <ErrorBoundary>
          <ExampleWithError {...this.props} />
        </ErrorBoundary>
        </Fragment>
      )
    }
}

export default ForgotPasswordView;
