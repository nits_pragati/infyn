import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';

class Home extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            selectedTopicId: "",
            currentPage: 0
        }
        this.goToApplyNow = this.goToApplyNow.bind(this);
       
    }
    goToApplyNow()
    {
        this.props.history.push('/applynow');
    }
    render() {
        return (
            <div>
                <section className="home-banner">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9 col-md-9 ml-auto mr-auto">
                                <div className="home-banner-text">
                                    <h2>Our Mission is to provide fair credit opportunities for international professionals</h2>
                                    <h6 className="mb-3">You are more than your credit score. We think your education and professional experience should get lower rates for you.</h6>
                                    <button type="button" onClick={this.goToApplyNow} className="btn btn-outline-light text-uppercase rounded-btn btn-lg mb-2">APPLY NOW!</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="how-it">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 mb-5">
                                <h1 className="text-center text-medium">How it works</h1>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-4 col-md-4 col-sm-4 text-center mb-4">
                                <div className="how-image mb-3">
                                    <img src={require('../../../../assets/img/how-1.png')} alt="" />
                                </div>
                                <h3 className="text-medium">Apply</h3>
                                <p className="text-secondary">Fill out application designed for immigrants. Our platform is free to use and it won’t impact your credit score to apply.</p>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-4 text-center mb-4">
                                <div className="how-image mb-3">
                                    <img src={require('../../../../assets/img/how-2.png')} alt="" />
                                </div>
                                <h3 className="text-medium">Get offers</h3>
                                <p className="text-secondary">Our platform matches you with loan offers from several lenders instantly.</p>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-4 text-center">
                                <div className="how-image mb-3">
                                    <img src={require('../../../../assets/img/how-3.png')} alt="" />
                                </div>
                                <h3 className="text-medium">Get funded</h3>
                                <p className="text-secondary">Choose the best offer with a low rate and get funded.</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="why">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 mb-5">
                                <h1 className="text-center text-medium">Why beneficial for immigrants</h1>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-4 col-md-4 col-sm-4 text-center mb-4">
                                <div className="why-image mb-3">
                                    <img src={require('../../../../assets/img/why-1.png')} alt="" />
                                </div>
                                <h4 className="text-medium">Quick and Easy</h4>
                                <p className="text-secondary">Few seconds to fill out application and based on your profile you get multiple offers within minutes</p>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-4 text-center mb-4">
                                <div className="why-image mb-3">
                                    <img src={require('../../../../assets/img/why-2.png')} alt="" />
                                </div>
                                <h4 className="text-medium">Totally Free</h4>
                                <p className="text-secondary">Infyn is truly free. No hidden fees</p>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-4 text-center">
                                <div className="why-image mb-3">
                                    <img src={require('../../../../assets/img/why-3.png')} alt="" />
                                </div>
                                <h4 className="text-medium">No Impact on credit score</h4>
                                <p className="text-secondary">We will never pull your credit to match you with offers. Your credit score will never be impacted for using our platform.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>




        )
    }
}


export default Home;
