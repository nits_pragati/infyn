import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
// import DatePicker from 'react-bootstrap-date-picker'
// import DatePicker from '../../../../../node_modules/react-bootstrap-date-picker/src/index.jsx';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import _ from 'lodash';
import { getSignupApi } from '../../api/usersApi';
import { jsonToFormData } from '../../entity/jsonToFormData';
import { NotificationManager } from 'react-notifications';
import VerificationForm from '../Verification/Form';
import CallingCodes from '../../entity/callingCodes';
import { getClientDetails } from '../../api/commonApi';
import { actions as loginActions } from '../../../redux/modules/login';
import { loginSelector } from '../../../redux/selectors/loginSelector';

class Signup extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      states: [],
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      phoneNumber: '',
      gender: 'M',
      birthDate: moment(),
      countries: '',
      state: '',
      city: '',
      street: '',
      zipCode: '',
      countryNameSet: '',

      //countryCodeSet: '',

      stateNameSet: '',
      formerrors: {},
      currentStep: 1,
      file: '',

      result: ''      

    }

    this.handleChange = this.handleChange.bind(this);

    this.handleChangeDate = this.handleChangeDate.bind(this);

    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleStateChange = this.handleStateChange.bind(this);

    this.handleAddressChange = this.handleAddressChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    // this.handleValidation = this.handleValidation.bind(this);


    const { signup } = this.props;
  }



  handleChangeDate(date) {
    this.setState({
      birthDate: date,
    });
  }

  
  handleCountryChange(event) {
    console.log('target ', event.target)
    if (event.target.value) {
      this.state.country = event.target.value;
      this.props.getStateList(event.target.value);
      console.log('c  ', this.state.countries.data, this.state.country)


      // For fetching country Name
      let countryName;
      this.state.countries.data.map((item) => {
        if (item.countryId.toString() == this.state.country) {
          countryName = item.name;
        }
      })
      if (countryName) {
        this.state.countryNameSet = countryName;
      }
      // For fetching country Name end

      const countryData = _.find(this.state.countries.data, { countryId: this.state.country });
      console.log('c data', countryData)
    }
  }


  handleStateChange(event) {
    if (event.target.value) {
      this.state.state = event.target.value;
      this.props.getCityList(event.target.value);
      console.log('Suman State', this.state.states.data, this.state.state)

      // For fetching state Name
      let stateName;
      this.state.states.data.map((item) => {
        if (item.stateId.toString() == this.state.state) {
          stateName = item.name;
        }
      })
      if (stateName) {
        this.state.stateNameSet = stateName;
      }
      // For fetching state Name end
    }
  }


  // handleCityChange(event)
  // {
  //   if(event.target.value)
  //   {
  //     this.state.city = event.target.value;

  //   }
  // }


// ////////////////////////////////// For Image Change Upload

fileChangedHandler = (event) => {
  console.log('Files ', event.target.files);
  console.log('File ', event.target.files['0']);
  const bodyFormData = new FormData();
  for (let x = 0; x < event.target.files.length; x++) {
    bodyFormData.append(`file[${x}]`, event.target.files[x]);
  }
  this.setState({ file: event.target.files['0'] })


  output.src = URL.createObjectURL(event.target.files[0]);
}

// ////////////////////////////////// For Image Change Upload


formatDate(date) {
  let d = new Date(date),
    month = `${d.getMonth() + 1}`,
    day = `${d.getDate()}`,
    year = d.getFullYear();

  if (month.length < 2) month = `0${month}`;
  if (day.length < 2) day = `0${day}`;

  return [month, day, year].join('/');
}


handleChange(event) {
  const change = {};
  change[event.target.name] = event.target.value;
  this.setState(change);
}

handleAddressChange(event) {
  const change = { address: {} };
  change[address[event.target.name]] = event.target.value;
  this.setState(change);
}

handleSubmit(event) {
  console.log(this.state);
  // let countryData = _.find(countries, {id:this.data.country});
  // console.log("c data",countryData)


  

  const resultData = {

    file: this.state.file,

    firstname: this.state.firstName,
    lastname: this.state.lastName,
    email: this.state.email,
    "phoneNumber": '+' + this.state.currentContryCallingCode + this.state.phoneNumber,
    gender: this.state.gender,
    birthDate: this.formatDate(this.state.birthDate),
    deviceId: '',
    deviceType: 'web',
    'address.address': this.state.address,
    'address.addressLine2': this.state.addressLine2,
    'address.city': this.state.city,
    'address.state': this.state.stateNameSet,
    'address.zipCode': this.state.zipCode,
    'address.country': this.state.countryNameSet,
    type: '',

    'socialAccounts.id': '',
    'socialAccounts.appId': '',
    'socialAccounts.authToken': '',

    'userAuth.username': this.state.username,
    'userAuth.password': this.state.password,
    'userAuth.email': this.state.email,

  }


  console.log('SUMANDATA FOR SIGNUP', resultData);
  if (this.state.file) {
    console.log('hiiiii', this.state.file)
    getSignupApi(jsonToFormData(resultData, {})).then((result) => {
      NotificationManager.success('Registration successfull.', 'Success');
      //this.setState({ currentStep: 2 });
      //this.forceUpdate();
      // this.props.history.push('/login');


// Fetch the successfull value
      this.setState ({
        result : result
      })
      console.log('MAIN THING',this.state.result)
      console.log('Fname',this.state.result.data.data.firstname)
      localStorage.setItem('userData', JSON.stringify(this.state.result.data.data));
// localStorage.setItem("firstname",JSON.stringify(this.state.result.data.data.firstname));

// localStorage.setItem("lastname",JSON.stringify(this.state.result.data.data.lastname));

// localStorage.setItem("gender",JSON.stringify(this.state.result.data.data.gender));

// localStorage.setItem("country",JSON.stringify(this.state.result.data.data.address.country));

// localStorage.setItem("fullpath",JSON.stringify(this.state.result.data.data.fullpath));

//console.log('LOCAL GET VALUE',localStorage.getItem('firstname'));

// Fetch the successfull value
      this.props.isLoggedIn();

  this.setState({ currentStep: 2 });
  this.forceUpdate();


    }, (error) => {
      NotificationManager.error('', error.response.data.message);
      // console.log(' 7777777777777 ',error.response)
    })
  } else {
    NotificationManager.error('Please upload image.', 'Error');
  }

  // this.props.submitSignup(resultData);
  event.preventDefault();
  event.stopPropagation();


  // ///////// For Blank the form input data

  const firstName = this.state.firstName;
  const lastName = this.state.lastName;
  const username = this.state.username;
  const email = this.state.email;
  const password = this.state.password;
  const phoneNumber = this.state.phoneNumber;
  const gender = this.state.gender;
  const countryNameSet = this.state.countryNameSet;
  const addressLine2 = this.state.addressLine2;
  const address = this.state.address;
  const stateNameSet = this.state.stateNameSet;
  const city = this.state.city;
  const zipCode = this.state.zipCode;
  const file = this.state.file;

  // this.setState({
  //   firstName: '',
  //   lastName: '',
  //   username: '',
  //   email: '',
  //   password: '',
  //   phoneNumber: '',
  //   gender: '',
  //   countryNameSet: '',
  //   addressLine2: '',
  //   address: '',
  //   stateNameSet: '',
  //   city: '',
  //   zipCode: '',
  //   file: ''
  // });

  // ///////// For Blank the form input data
}

componentDidMount() {
  getClientDetails().then((result) => {
    if (result.status = 200)
    {
      console.log('Country Phone Number Result',result);
      //console.log('Country Phone Calling Code',result.data.calling_code);
      
      this.setState({
        currentCuntry: result.data.country_code,
        currentContryCallingCode: result.data.calling_code      
      })
      console.log('This is country Code By me',this.state.currentCuntry);

      
    }
  })
  // getClientDetails().then((result) => {
  //   if (result.status = 200)
  //   {
  //     this.setState({ currentCuntry: result.data.country_code})      
  //     console.log('This is country Code By me',this.state.currentCuntry);
  //   }
  // })
}
donorSkip() {
  this.props.history.push('/');
}
onSkipVerification() {
  this.setState({ currentStep: 3 });
  this.forceUpdate();
}
onNext(id) {
  this.setState({ currentStep: id });
  this.forceUpdate();
}

render() {
  // let countries = {};
  // let states = {};
  let cities = {};
  const { signup } = this.props;
  console.log('new home ', signup);
  let statusMessages = {};
  if (signup.signup_error) {
    statusMessages = signup.signup_error.toJS();
    console.log('signup status ', statusMessages)
  }
  if (signup.countries) {
    this.state.countries = signup.countries.toJS();
  }

  if (signup.states) {
    this.state.states = signup.states.toJS();
  }

  if (signup.cities) {
    cities = signup.cities.toJS();
  }

  console.log(cities);
  //  const result = home && home.result ? home.result : null;
      console.log("STST",this.state.states);
    //  const result = home && home.result ? home.result : null;


  return (
    <div>
      {/* <section className="banner-signup d-flex" style={{ backgroundImage: "url(" + require('../../../../assets/img/signup-banner.jpg') + ")" }}>
                    <div className="container align-self-center">
                        <h2 className="text-white">Register</h2>
                    </div>
                </section> */}
      <section className="py-5">
        <div className="container text-center">
          <h2 className="font-weight-bold black-text">Create an Account</h2>
          <p className="black-text my-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br />Lorem Ipsum has been the industry's standard dummy.</p>
          <div>
            <div className="row justify-content-center mt-5">
              <div className="col-md-9">
                <ul className="steps">
                  <li className={this.state.currentStep == 1 ? 'active' : ''}>
                    <a href="javascript:void(0);">
                      <span>Signup</span>
                    </a>
                  </li>
                  <li className={this.state.currentStep == 2 ? 'active' : ''}>
                    <a href="javascript:void(0);">
                      <span>Verification</span>
                    </a>
                  </li>
                  <li className={this.state.currentStep == 3 ? 'active' : ''}>
                    <a href="javascript:void(0);">
                      <span>Become a Donor</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="row justify-content-center mt-5">
            {this.state.currentStep == 1 ? (
              <div className="col-lg-12 col-sm-12">
                <div className="card p-5 text-left">
                  <div className="row">
                    <div className="col-lg-6 col-sm-12">
                      <b className="register-text mb-3 d-block">Register for Free</b>
                      <form className="signup" onSubmit={this.handleSubmit}>
                        <div className="form-row">

                          <div className="form-group col-md-3 m-auto position-relative">
                                  <img id="output" className="rounded-circle" width="100px" height="100px" src={require('../../../../assets/img/profile.png')} />
                                  <div className="register-image position-absolute">
                                      <input type="file" className="register-image-file" id="validatedCustomFile" onChange={this.fileChangedHandler} />
                                      <label className="register-image-icon"><i className="fa fa-camera" /></label>
                                    </div>
                                </div>


                          <div className="input-group col-md-12 mb-3 mt-3">
                                  <div className="input-group-prepend">
                                      <img src={require('../../../../assets/img/username.png')} alt="" />
                                    </div>
                                  <input type="text" className="form-control" name="firstName" onChange={this.handleChange.bind(this)} value={this.state.firstName} required placeholder="First Name" />
                                </div>
                          <div className="input-group col-md-12 mb-3">
                                  <div className="input-group-prepend">
                                      <img src={require('../../../../assets/img/username.png')} alt="" />
                                    </div>
                                  <input type="text" className="form-control" id="lastname" name="lastName" onChange={this.handleChange.bind(this)} value={this.state.lastName} required placeholder="Last Name" />
                                </div>
                          <div className="input-group col-md-12 mb-3">
                                  <div className="input-group-prepend">
                                      <img src={require('../../../../assets/img/username.png')} alt="" />
                                    </div>
                                  <input type="text" className="form-control" id="username" name="username" onChange={this.handleChange.bind(this)} value={this.state.username} required placeholder="Username" />
                                </div>
                          <div className="input-group col-md-12 mb-3">
                                  <div className="input-group-prepend">
                                      <img src={require('../../../../assets/img/email.png')} alt="" />
                                    </div>
                                  <input type="email" className="form-control" id="email" name="email" onChange={this.handleChange.bind(this)} value={this.state.email} required placeholder="Email" />
                                </div>

                          <div className="input-group col-md-12 mb-3">
                            <div className="input-group-prepend">
                                <img src={require('../../../../assets/img/password.png')} alt="" />
                              </div>
                            <input type="password" className="form-control" placeholder="Password" aria-label="Username" aria-describedby="basic-addon1" name="password" onChange={this.handleChange.bind(this)} value={this.state.password} required />
                          </div>


                          <div className="input-group col-md-12 mb-3">
                            <div className="input-group-prepend">
                                <img src={require('../../../../assets/img/phone.png')} alt="" />
                            </div>

                            {/* <select placeholder="Country Code" name="currentCuntry" className="form-control" value={this.state.currentCuntry && this.state.currentCuntry ? this.state.currentCuntry : ''} onChange={this.handleChange.bind(this)}>
                              {CallingCodes ? CallingCodes.map((data, key) => {
                                return (<option key={key} value={data.code}>{data.country}(+{data.value})</option>);
                              }) : ''}
                            </select> */}

                            <select placeholder="Country Code" name="currentCuntry" className="form-control" value={this.state.currentCuntry && this.state.currentCuntry ? this.state.currentCuntry : ''} onChange={this.handleChange.bind(this)}>
                              {CallingCodes ? CallingCodes.map((data, key) => {
                                return (<option key={key} value={data.code}>{data.country}(+{data.value})</option>);
                              }) : ''}
                            </select>




                             <input type="phone" className="form-control ml-3" placeholder="Phone Number" name="phoneNumber" onChange={this.handleChange.bind(this)} value={this.state.phoneNumber} required /> 
                          </div>



                          {/* <div className="input-group col-md-12 mb-3">
                                                <div className="input-group-prepend">
                                                  <img src={require('../../../../assets/img/username.png')} alt="" />
                                                </div>
                                                <DatePicker className="form-control" name="birthDate" placeholderText="Date Of Birth"  />
                                              </div> */}


                                <div className="input-group col-md-12 mb-3">
                                  {/* <label className="bmd-label-static">Date of Birth</label> */}
                                  <div className="input-group-prepend">
                                      <img src={require('../../../../assets/img/calender.png')} alt="" />
                                  </div>

                                  <DatePicker
                                                      // dateFormat="MM/DD/YYYY"
                                      className="form-control"
                                      selected={moment(this.state.birthDate)}
                                      onChange={this.handleChangeDate}
                                                      // minDate={moment(this.state.birthDate)}
                                      placeholderText="Date Of Birth (MM/DD/YYYY)"
                                      formatDate={date => moment(date).format('MM/DD/YYYY')}
                                    />

                                </div>

                                <div className="input-group col-md-12 mb-3">
                                  <div className="input-group-prepend">
                                      <img src={require('../../../../assets/img/username.png')} alt="" />
                                    </div>
                                  <select className="form-control" name="gender" onChange={this.handleChange.bind(this)} value={this.state.gender} required>
                                      <option value="M">Male</option>
                                      <option value="F">Female</option>
                                    </select>
                                </div>

                                <div className="input-group col-md-12 mb-3">
                                  <div className="input-group-prepend">
                                      <img src={require('../../../../assets/img/location.png')} alt="" />
                                    </div>
                          
                                  <select className="form-control" name="country" onChange={this.handleCountryChange} required>
                                      <option>Country</option>
                                      {this.state.countries && this.state.countries.data ? this.state.countries.data.map((data, key) => {
                                                    return <option key={key} value={data.countryId}>{data.name}</option>
                                                  }) : ''}

                                    </select>
                                </div>


                          <div className="input-group col-md-12 mb-3">
                                  <div className="input-group-prepend">
                                      <img src={require('../../../../assets/img/location.png')} alt="" />
                                    </div>
                                  <input type="phone" className="form-control" placeholder="Street" name="address" onChange={this.handleChange.bind(this)} value={this.state.address} required />
                                </div>


                          <div className="input-group col-md-12 mb-3">
                                  <div className="input-group-prepend">
                                      <img src={require('../../../../assets/img/location.png')} alt="" />
                                    </div>
                                  <select className="form-control" name="state" onChange={this.handleStateChange} required>
                                      <option>State</option>
                                      {this.state.states && this.state.states.data ? this.state.states.data.map((data, key) => {
                                                    return <option key={key} value={data.stateId}>{data.name}</option>
                                                  }) : ''}

                                    </select>
                                  {/* <i class="fa fa-circle-o-notch fa-spin registration-spinner" ></i> */}
                                </div>


                          <div className="input-group col-md-12 mb-3">
                                  <div className="input-group-prepend">
                                      <img src={require('../../../../assets/img/location.png')} alt="" />
                                    </div>
                                  <select className="form-control" name="city" onChange={this.handleChange.bind(this)} placeholder="Password" required>
                                      <option>City</option>
                                      {cities.data ? cities.data.map((data, key) => {
                                                  return <option key={key} value={data.name}>{data.name}</option>
                                                }) : ''}
                                    </select>
                                </div>


                          <div className="input-group col-md-12 mb-3">
                                  <div className="input-group-prepend">
                                      <img src={require('../../../../assets/img/location.png')} alt="" />
                                    </div>
                                  <input type="text" className="form-control" name="zipCode" onChange={this.handleChange.bind(this)} value={this.state.zipCode} required placeholder="Postal" aria-label="Username" aria-describedby="basic-addon1" />
                                </div>


                          {/* ///////////////// Changes of Image /////////////////// */}


                          {this.props.signup.signup_error ? <p style={{ color: 'red' }}>You must fill the all field and provide your image</p> : ''}

                          {/* ///////////////// Changes of Image /////////////////// */}
                          <div className="input-group col-md-12 mb-3">
                                  <button type="submit" className="btn btn-primary btn-raised">Create an Account</button>
                                  {/* <button type="button" className="btn btn-primary btn-raised" onClick={this.onNext.bind(this, 2)}>Create an Account</button> */}
                                </div>
                        </div>


                      </form>


                    </div>
                    <div className="col-lg-1 col-sm-12">
                      <div className="vr">
                        <b>Or</b>
                      </div>
                    </div>
                    <div className="col-lg-5 text-center login-right-social col-sm-12">
                      <img src={require('../../../../assets/img/color_logo.png')} alt="" className="img-fluid" />
                      <ul className="my-4 mx-auto d-flex list-unstyled justify-content-center ">
                        <li className="facebook"><i className="fa fa-facebook" /></li>
                        <li className="google"><i className="fa fa-google-plus" /></li>
                      </ul>
                      <a className="d-block">Already a member?</a>

                      <Link to="/signup">
                        <button type="submit" className="btn btn-outline-primary mt-3">Login</button>
                      </Link>
                    </div>
                  </div>
                </div>

              </div>
                            ) : ''}
            {this.state.currentStep == 2 ? (
              <div className="col-lg-8 col-sm-8">
                <VerificationForm onSkipVerification={this.onSkipVerification.bind(this)} />
              </div>
                            ) : ''}
            {this.state.currentStep == 3 ? (
              <div className="col-lg-8 col-sm-8">
               
              </div>
                            ) : ''}

          </div>
        </div>
      </section>

    </div>
  );
}
}

const mapDispatchToProps = {
  ...loginActions,
};
const mapStateToProps = state => ({
  login: loginSelector(state),
});
export default connect(mapStateToProps, mapDispatchToProps)(Signup);