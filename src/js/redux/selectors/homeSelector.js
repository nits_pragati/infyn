import { createSelector } from 'reselect';

const homeDataSelector = state => state.home;

const resultSelector = createSelector(
  homeDataSelector,
  payload => payload.get('result')
  
);


const userDataTypeSelector = createSelector(
  homeDataSelector,
  payload => payload.get('resulttype')
);


export const homeSelector = state => ({
  result: resultSelector(state),
  resulttype: userDataTypeSelector(state),
});
