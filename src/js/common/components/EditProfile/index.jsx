import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import _ from 'lodash';
import { actions as loginActions } from '../../../redux/modules/login';
import { loginSelector } from '../../../redux/selectors/loginSelector';
import Sidebar from '../Sidebar';
import { jsonToFormData } from '../../entity/jsonToFormData';
import { submitProfile } from '../../api/usersApi';
import { NotificationManager } from 'react-notifications';

class EditProfile extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {

      city:'',
      country:'',      
      state:'',

      countries:[],      
      states:[],

      countryNameSet:'',
      stateNameSet:'',


 profile: {
        userId:'',
        firstname:'',
        lastname:'',
        email:'',
        gender:'',
        phoneNumber:'',
        birthDate:'',
        birthDate: moment(),
        address:{active:true,
        address:'',
        city:'',
        country:'',
        id:'',
        state:'',
        zipCode:'',
        userId:''},
        userAuth:{
          username:'',
          },
        userFiles:[],

    },
    initialized: false,
    }
    this.props.isLoggedIn();
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeCity = this.handleChangeCity.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAddressChange = this.handleAddressChange.bind(this);

    this.handleChangeDate = this.handleChangeDate.bind(this);
  //  this.jsonToFormData = this.jsonToFormData.bind(this);

  this.handleCountryChange = this.handleCountryChange.bind(this);
  this.handleStateChange = this.handleStateChange.bind(this);


  const { editprofile } = this.props;
  }


  handleChangeDate(date) {
      this.setState({
        birthDate: date
      });
    }




  handleCountryChange(event)
  {
    console.log("target ",event.target)
    if(event.target.value)
    {
      this.state.country = event.target.value;
      this.props.getStateList(event.target.value);
      console.log('c  ',this.state.countries.data,this.state.country)


                  //For fetching country Name
                    let countryName;
                    this.state.countries.data.map((item)=>{
                      if(item.countryId.toString()==this.state.country)
                      {
                        countryName=item.name;
                      }
                    })
                    if(countryName)
                    {
                      this.state.countryNameSet = countryName;
                      this.state.profile.address.country = countryName;
                    }
                  //For fetching country Name end

      let countryData = _.find(this.state.countries.data, {countryId:this.state.country});
      console.log("c data",countryData)

    }
  }

  handleStateChange(event)
  {
    if(event.target.value)
    {
      this.state.state = event.target.value;
      this.props.getCityList(event.target.value);
      console.log('Suman State',this.state.states.data,this.state.state)

                  //For fetching state Name
                    let stateName;
                      this.state.states.data.map((item)=>{
                        if(item.stateId.toString()==this.state.state)
                        {
                          stateName=item.name;
                        }
                      })
                      if(stateName)
                      {
                        this.state.stateNameSet = stateName;
                        this.state.profile.address.state = stateName;
                      }
                  //For fetching state Name end





    }
  }








  formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [month, day, year].join('/');
  }


  handleAddressChange(event) {
    let change = this.state.profile;
    console.log(change)
    change.address[event.target.name] = event.target.value;
    this.setState({ profile: change });
    this.forceUpdate()
  }

  handleChange(event) {
    let change = this.state.profile;
    console.log(' ---- ', change);
    change[event.target.name] = event.target.value;
    console.log(' +++ ', change);
    this.setState({ profile: change });
    this.forceUpdate()
  }

  componentDidMount() {
    let userData;
    if (this.props.login.userData) {
      console.log('User Data  ', this.props.login.userData.toJS())
      userData = this.props.login.userData.toJS();
      this.props.getEditProfile(userData.id);
      this.setState({ initialized: false });
    }

    //this.props.getCountries();
  }

  handleChangeCity = (event) => {
    this.state.profile.address.country = event.target.value;
    }

  fileChangedHandler = (event) => {
    this.state.profile.userFiles = event.target.files['0'];

    // this.setState({profile:this.state.profile});
      output.src = URL.createObjectURL(event.target.files[0]);
    }
  handleSubmit(event) {
    console.log('Before gotting Suman',this.state.profile);
    //this.state.profile.userId = this.state.profile.id;
    // this.setState({profile:this.state.profile});
    // this.forceUpdate();
    var ret_data = jsonToFormData(this.state.profile,{});
    console.log('I goT This ALL Value',ret_data);
    //.log('SUMANDATA', resultData);
    // this.props.submitSignup(resultData);
    submitProfile(this.state.profile.userId,ret_data).then(result => {
      //this.state.profile.profileImg = '';
      NotificationManager.success('Profile updated successfully.', 'Success');
      this.props.getEditProfile(this.state.profile.userId)
    })
    event.preventDefault();
    event.stopPropagation();
  }

  getImageURI() {
    return this.state.profile.profileImg;
  }


  render() {
    
    let cities = {};

    const { editprofile } = this.props;
    console.log('user data', editprofile)
    let profileDetails;
    let i = false;
    if (editprofile.profile && !this.state.initialized) {
      profileDetails = editprofile.profile.toJS();
     profileDetails = profileDetails.data;
      var tempProfile = {
             userId:profileDetails.id,
             firstname:profileDetails.firstname,
             lastname:profileDetails.lastname,
             email:profileDetails.email,
             gender:profileDetails.gender,
             phoneNumber:profileDetails.phoneNumber,
             birthDate:profileDetails.birthDate,

             address:{
              active:true,
              address:profileDetails.address.address,
              city:profileDetails.address.city,
              country:profileDetails.address.country,
              id:profileDetails.address.id,
              state:profileDetails.address.state,
              zipCode:profileDetails.address.zipCode,
              //userId:profileDetails.address.userId
            },

            userAuth:{
              username:profileDetails.userAuth.username,              
            },

             profileImg:__CONFIG__.user_image + profileDetails.profileImg + "?" + Math.random(),
             userFiles:[]
         }
//          fetch(tempProfile.profileImg.replace('https','http'))
//   .then(res => res.blob()) // Gets the response and returns it as a blob
//   .then(blob => {
//   console.log('blob -- ',blob)
// });
        //  var reader  = new FileReader();
         //
        //   reader.onloadend = function () {
        //     console.log(reader.result); //this is an ArrayBuffer
        //     profileImg.userFiles(reader.result);
        //     this.setState({profile:tempProfile});
        //   }
        //   reader.readAsDataURL(tempProfile.profileImg.replace('https','http'));

      //   var blob = null;
      // var xhr = new XMLHttpRequest();
      // xhr.open("GET", tempProfile.profileImg.replace('https','http'));
      // xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
      // xhr.onload = function()
      // {
      //   console.log('1111111111111 ============== ',xhr.response)
      //     blob = xhr.response;//xhr.response is now a blob object
      // }
      // xhr.send();
         //
        //  console.log('///////////////////',tempProfile);
      //this.state.profile = tempProfile
      this.setState({profile:tempProfile});
      i = true;
      this.setState({ initialized: true });
    }


    if(editprofile.countries)
    {
        this.state.countries = editprofile.countries.toJS();
    }
    if(editprofile.states)
    {
      this.state.states = editprofile.states.toJS();
    }

    if(editprofile.cities)
    {
      cities = editprofile.cities.toJS();
    }


    console.log('STSTST',this.state.states)

    console.log('state --- ', this.state);
    return (
          <div>


              <section className="py-4">
                  <div className="container">
                      <div className="row">

  <Sidebar />


  <div className="col-md-9">
                          <div className="card dashboard-panel">
  <div className="card-header">Update Your Profile</div>

  <div className="card-body">


                                  <form className="signup" method="post" onSubmit={this.handleSubmit}>
                                  <div className="form-group row">
                                    <label className="bmd-label-static col-sm-3">First Name</label>
                                    <input type="text" className="form-control col-sm-5" name="firstname" onChange={this.handleChange.bind(this)} value={this.state.profile.firstname} placeholder="Enter your First Name" />
                                  </div>
                                  <div className="form-group row">
                                    <label className="bmd-label-static col-sm-3">Last Name</label>
                                    <input type="text" className="form-control col-sm-5" name="lastname" onChange={this.handleChange.bind(this)} value={this.state.profile.lastname} placeholder="Enter your Last Name" />
                                  </div>

                                  <div className="form-group row">
                                    <label className="bmd-label-static col-sm-3">Username</label>
                                    <input type="text" className="form-control col-sm-5" name="lastname" onChange={this.handleChange.bind(this)} value={this.state.profile.userAuth.username} placeholder="Enter your Username" />
                                  </div>

                                  <div className="form-group row">
                                    <label className="bmd-label-static col-sm-3">Email</label>
                                    <input type="email" className="form-control col-sm-5" name="email" onChange={this.handleChange.bind(this)} value={this.state.profile.email} placeholder="Enter your Email" />
                                  </div>


<div className="form-group row">
<label className="bmd-label-static col-sm-3">Date of Birth</label>

    <DatePicker
        //dateFormat="MM/DD/YYYY"
        className="form-control col-sm-5"
        selected={moment(this.state.profile.birthDate)}
        onChange={this.handleChangeDate}
        //minDate={moment(this.state.birthDate)}
        placeholderText="Date Of Birth (MM/DD/YYYY)"
        formatDate={(date) => moment(date).format('MM/DD/YYYY')}
    />
</div>



<div className="form-group row">
    <label className="bmd-label-static col-sm-3">Phone Number</label>
    <input type="text" className="form-control col-sm-5" name="address" onChange={this.handleChange.bind(this)} value={this.state.profile.phoneNumber} placeholder="Enter your Address" />
</div>




                                  <div className="form-group row">
                                    <label className="bmd-label-static col-sm-3">Gender</label>
                                    <select className="form-control col-sm-5" name="gender" onChange={this.handleChange.bind(this)} value={this.state.profile.gender} required>
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                    </select>
                                  </div>



                                  {/* <div className="form-group row">
                                    <label className="bmd-label-static col-sm-3">Change profile photo</label>
                                    <div className="custom-file col-sm-5">
                                            <input className="custom-file-input" required="" type="file" />
                                            <label className="custom-file-label">Upload photos</label>
                                        </div>
                                </div>*/}

                                

<div className="form-group row">
  <label className="bmd-label-static col-sm-3">Country</label>
  <select className="form-control col-sm-5" name="country" onChange={this.handleCountryChange} >
    <option>{this.state.profile.address.country}</option>
    {this.state.countries && this.state.countries.data?this.state.countries.data.map((data,key) =>{
      return <option key={key} value={data.countryId}>{data.name}</option>
    }
    ):''}
  </select>
</div>




<div className="form-group row">
  <label className="bmd-label-static col-sm-3">State</label>
  <select className="form-control col-sm-5" name="state" onChange={this.handleStateChange} required>
    <option>{this.state.profile.address.state}</option>
    {this.state.states && this.state.states.data?this.state.states.data.map((data,key) =>{
      return <option key={key} value={data.stateId}>{data.name}</option>
    }
    ):''}

  </select>
</div>



<div className="form-group row">
  <label className="bmd-label-static col-sm-3">City</label>  
  <select className="form-control col-sm-5" name="city" onChange={this.handleAddressChange.bind(this)} placeholder="City" required>
    <option>{this.state.profile.address.city}</option>
    {cities.data?cities.data.map((data,key) =>{
      return <option key={key} value={data.name}>{data.name}</option>
    }
    ):''}
  </select>
</div>


                                {/* <div className="form-group row">
                                    <label className="bmd-label-static col-sm-3">State</label>
                                    <input type="text" className="form-control col-sm-5" name="state" onChange={this.handleAddressChange.bind(this)} value={this.state.profile.address.state} placeholder="Enter State" />
                                </div>

                                <div className="form-group row">
                                    <label className="bmd-label-static col-sm-3">City</label>
                                    <input type="text" className="form-control col-sm-5" name="city" onChange={this.handleAddressChange.bind(this)} value={this.state.profile.address.city} placeholder="Enter your City" />
                                </div> */}



                                <div className="form-group row">
                                    <label className="bmd-label-static col-sm-3">address</label>
                                    <input type="text" className="form-control col-sm-5" name="address" onChange={this.handleAddressChange.bind(this)} value={this.state.profile.address.address} placeholder="Enter your Address" />
                                </div>

                                <div className="form-group row">
                                    <label className="bmd-label-static col-sm-3">Zip Code</label>
                                    <input type="text" className="form-control col-sm-5" name="zipCode" onChange={this.handleAddressChange.bind(this)} value={this.state.profile.address.zipCode} placeholder="Enter Country" />
                                </div>

                                <div className="form-group row">
                                    <label className="bmd-label-static col-sm-3">Upload Photo</label>
                                    <div className="custom-file col-sm-5">
                                    <input type="file" className="custom-file-input" id="validatedCustomFile" onChange={this.fileChangedHandler} />
                                    <label className="custom-file-label">Upload Photo</label>
                                    </div>
                                </div>
                                {
                                  this.state.profile.profileImg?(
                                      <div className="form-group row">
                                        <label className="bmd-label-static col-sm-3">&nbsp;</label>
                                        <img id="output" src={this.getImageURI()} className="col-sm-5" height="200"/>
                                      </div>
                                  ):''
                                }

                                  <div className="form-group row">
                                      <button type="submit" className="btn btn-primary btn-raised px-3 offset-sm-3">Save Changes</button>
                                    </div>
                                </form>


                                </div>

             </div>
                        </div>


               </div>
                    </div>
                </section>


            </div>);
  }
}

const mapDispatchToProps = {
  ...loginActions,
};
const mapStateToProps = state => ({
  login: loginSelector(state),
});
export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
