import { createSelector } from 'reselect';

const forgotpasswordDataSelector = state => state.forgotpassword;

const resultSelector = createSelector(
  forgotpasswordDataSelector,
  payload => payload.get('result')
);

export const forgotpasswordSelector = state => ({
  result: resultSelector(state),
});