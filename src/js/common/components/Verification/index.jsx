import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import VerificationForm from './Form'
import Sidebar from '../Sidebar';

class EditProfile extends PureComponent {
    constructor(props) {
        super(props);


    }




    render() {
        return (
            <div>
                <section className="py-4">
                    <div className="container">
                        <div className="row">
                            <Sidebar />
                            <div className="col-md-9">
                                <VerificationForm />
                            </div>
                        </div>
                    </div>
                </section>
            </div>
           );
    }
}

export default EditProfile;
