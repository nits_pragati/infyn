import { put, fork, takeLatest, call } from 'redux-saga/effects';
import { constants as loginConstants, actions as loginActions } from '../modules/login';
import {browserHistory} from 'react-router';
import type { signupType } from '../../common/types/login';
import axios from "axios";

let localStorage

// If we're testing, use a local storage polyfill
if (global.process && process.env.NODE_ENV === 'test') {
  localStorage = require('localStorage')
} else {
  // If not, use the browser one
  localStorage = global.window.localStorage
}

function loginApi(data)
{
  console.log('SUMAN LOGINAPIII',data);
  return axios({
        method: "POST",
        //url: "https://dog.ceo/api/breeds/image/random"
        headers: { "Content-type": "application/json" },
        url:__CONFIG__.api_url + "auth/login",
        data: data.data
    }).then(response => response.data
    );
}
export function* checkLogin(actions) {

  try {
          const response = yield call(loginApi,actions.payload);
          //const dog = response.data.message;
          console.log('checking sync ',response);
          //localStorage.userData = response;
          localStorage.setItem('userData', JSON.stringify(response.data));
          // dispatch a success action to the store with the new dog
          yield put(loginActions.loginReturn(response));
          yield put(loginActions.setLoginDetails(response.data));

          yield put(loginActions.loginError(false));
          //forwardTo('/editprofile');
          //browserHistory.push('/editprofile');
      } catch (error) {
          // dispatch a failure action to the store with the error
          //yield put({ type: "API_CALL_FAILURE", error });
          console.log('error',error);
          yield put(loginActions.loginError(true));
      }

}

export function* getLoginDetails() {
  console.log('local  ',localStorage);
  var userData = localStorage.getItem('userData');
  console.log('local  1',userData);
  if(userData)
  {
    yield put(loginActions.setLoginDetails(JSON.parse(userData)));
  }
  else {
    yield put(loginActions.setLoginDetails(false));
  }

}

export function* removeLoginDetails() {
  var userData = localStorage.removeItem('userData');
  yield put(loginActions.setLoginDetails(false));
}

function* watchGetLogin() {
  yield takeLatest(loginConstants.SUBMIT_LOGIN, checkLogin);
  yield takeLatest(loginConstants.IS_LOGGED_IN, getLoginDetails);
  yield takeLatest(loginConstants.USER_LOGOUT, removeLoginDetails);
}

export const loginSaga = [
  fork(watchGetLogin),
];
