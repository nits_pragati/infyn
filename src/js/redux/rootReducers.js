import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import example from './modules/example';
import home from './modules/home';
import signup from './modules/signup';
import login from './modules/login';
import forgotpassword from './modules/forgotpassword';
import editprofile from './modules/editprofile';
export default combineReducers({
  example,
  routing,
  home,
  signup,
  login,
  forgotpassword,
  editprofile
});
