import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import LazyLoading from '../../common/components/LazyLoading/LazyLoading';
import { actions as helpActions } from '../../redux/modules/help';
import { helpSelector } from '../../redux/selectors/helpSelector';
import { ExampleWithError } from '../../common/components/Example';
import { ErrorBoundary } from '../../common/components/Utilities';
const LazySample = Loadable({
    loader: () => import('../../common/components/help'),
    loading: LazyLoading,
})
const mapDispatchToProps = {
    ...helpActions,
};
const mapStateToProps = state => ({
    home: helpSelector(state),
});
@connect(mapStateToProps, mapDispatchToProps)

class HelpView extends Component {
    static propTypes = {
        //home: PropTypes.object.isRequired,
    }

    componentDidMount() {
    }
    render() {
        return (
            <Fragment>
                <LazySample {...this.props} />
                <ErrorBoundary>
                    <ExampleWithError {...this.props} />
                </ErrorBoundary>
            </Fragment>
        )
    }
}

export default HelpView;
