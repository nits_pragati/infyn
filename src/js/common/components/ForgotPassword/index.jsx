import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';

import { jsonToFormData } from '../../entity/jsonToFormData';
import {getForgotpasswordApi} from '../../api/usersApi';
import { NotificationManager } from 'react-notifications';


class ForgotPassword extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
          email:'',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }



    handleChange(event) {
      var change = {};
      change[event.target.name] = event.target.value;
      this.setState(change);
    }


    handleSubmit(event) {
      console.log(this.state);
      // let countryData = _.find(countries, {id:this.data.country});
      // console.log("c data",countryData)


      let resultData= {
        "email": this.state.email,
      }

      console.log('SUMANDATAFOR_Forgotpassword',resultData);
      
      


      getForgotpasswordApi(jsonToFormData(resultData,{})).then(result => {
              NotificationManager.success('Password sent to your mail id', 'Success');
              //this.props.history.push("/login");
            },error=>{
              NotificationManager.error('error!!!', error.response.data.message);
              //console.log(' 7777777777777 ',error.response)
            })



      //this.props.submitForgotPassword(resultData);
      event.preventDefault();
      event.stopPropagation();

      
      }

    

    render() {
        return (
            
          <div>
          <section className="banner-signup d-flex" style={{ backgroundImage: "url(" + require('../../../../assets/img/signup-banner.jpg') + ")" }}>
              <div className="container align-self-center">
                  <h2 className="text-white">Forgot Password </h2>
              </div>
          </section>
          <section className="py-5">
            <div className="container text-center">
              <h2 className="font-weight-bold black-text">Forgot Password</h2>
              <p className="black-text my-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br />Lorem Ipsum has been the industry's standard dummy.</p>
              <div className="row justify-content-center mt-5 login">
                <div className="col-md-10">
                  <div className="card p-5 text-left">
                    <div className="row">
                      <div className="col-lg-6 col-sm-12">
                        <b className="register-text mb-3 d-block">Forgot your Password?</b>
                        
                        <form onSubmit={this.handleSubmit}>

                          <div className="input-group mb-3">
                            <div className="input-group-prepend">
                              <img src={require('../../../../assets/img/username.png')} alt="" />
                            </div>
                            <input type="text" className="form-control" placeholder="Email" aria-label="Username" aria-describedby="basic-addon1" name="email" onChange={this.handleChange.bind(this)} value={this.state.email} required />
                          </div>                           

                          <button type="submit" className="btn btn-primary btn-raised">Submit</button>                         

                        </form>

                      </div>
                      <div className="col-lg-1 col-sm-12">
                        <div className="vr">
                          <b>Or</b>
                        </div>
                      </div>
                      <div className="col-lg-5 text-center login-right-social col-sm-12">
                        <img src={require('../../../../assets/img/color_logo.png')} alt="" className="img-fluid"/>
                        <ul className="my-4 mx-auto d-flex list-unstyled justify-content-center ">
                          <li className="facebook"><i className="fa fa-facebook"></i></li>
                          <li className="google"><i className="fa fa-google-plus"></i></li>
                        </ul>
                        {/* <a className="d-block">Login here</a> */}

                        <Link to="/">
                        <button type="submit" className="btn btn-outline-primary mt-3">Sign In Here</button>
                        </Link>
                        

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

      </div>


            );
    }





appendInput() {
    var newInput = `input-${this.state.inputs.length}`;
    this.setState({ inputs: this.state.inputs.concat([newInput]) });
}

appendInputItem() {
    var newInputitem = `inputitem-${this.state.inputsitem.length}`;
    this.setState({ inputsitem: this.state.inputsitem.concat([newInputitem]) });
}





}

export default ForgotPassword;

