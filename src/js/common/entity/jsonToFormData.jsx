export function jsonToFormData (inJSON, inTestJSON, inFormData=null, parentKey=null) {
  // http://stackoverflow.com/a/22783314/260665
  // Raj: Converts any nested JSON to formData.
 // console.log("krish",inJSON);
  var form_data = inFormData || new FormData();
  var testJSON = inTestJSON || {};
  for ( var key in inJSON ) {
      // 1. If it is a recursion, then key has to be constructed like "parent.child" where parent JSON contains a child JSON
      // 2. Perform append data only if the value for key is not a JSON, recurse otherwise!
      var constructedKey = key;
      if (parentKey) {
          constructedKey = parentKey + "." + key;
      }

      var value = inJSON[key];
      if (value && value.constructor === {}.constructor) {
          // This is a JSON, we now need to recurse!
          jsonToFormData (value, testJSON, form_data, constructedKey);
      } else {
          form_data.append(constructedKey, inJSON[key]);
          testJSON[constructedKey] = inJSON[key];
      }
  }
  //console.log("krish2",form_data);
  
  return form_data;
}
