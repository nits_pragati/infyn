import axios from 'axios';

const headers = {
  'accept': 'application/json',

  'X-CSRFToken': 'NwBsh2mrFVzcR0zZHHdfJ8C8zPDJKefgwnn4m0TigIwGwbuqksc4VIjpN7T5av0g'
}
export function getClientDetails() {
  return axios({
    method: 'GET',
    url: 'https://jsonplaceholder.typicode.com/posts',
  })
}


export function getAboutUsDetails() {
  return axios.get(__CONFIG__.api_url + "contents/about-us/", {
    headers: headers
  })
}

export function getBusinessList() {
  return axios.get(__CONFIG__.api_url + "companies/", {
    headers: headers
  })
}


export function applyNow(data) {
  if (data.modalIsOpen) {
    delete data.modalIsOpen
  }
  if(data.qrcode)
  {
    delete data.qrcode
  }

  return axios({
    method: "POST",
    config: {
      headers: headers
    },
    url: __CONFIG__.api_url + "waitings/create/",
    data: data
  })
}

export function validateOfferCode(offerCode) {
  return axios.get(__CONFIG__.api_url + "codes/"+offerCode+"/", {
    headers: headers
  })
}


export function postApplication(data) {
  

  return axios({
    method: "POST",
    config: {
      headers: headers
    },
    url: __CONFIG__.api_url + "applications/create/",
    data: data
  })
}

export function postHelpUs(data) {


  return axios({
    method: "POST",
    config: {
      headers: headers
    },
    url: __CONFIG__.api_url + "helps/create/",
    data: data
  })
}
