import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

class Contact extends PureComponent {
    constructor(props) {
        super(props);

       
    }

  


    render() {
        return (
            <div>
               <section className="explore-head d-flex" style={{ backgroundImage: "url(" + require('../../../../assets/img/contact.jpg') + ")" }}>
                  <div className="container align-self-center">
                      <h1 className="text-white text-center d-block">Contact Us</h1>                     
                  </div>
                </section>
                 <section className="contact-body py-5">
                    <div className="container">                        
                        <div className="row">
                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-12">
                                        <h2>Contact Us</h2>
                                        <p className="my-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                    </div>
                                    <div className="col-md-6">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2965.0824050173574!2d-93.63905729999999!3d41.998507000000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sWebFilings%2C+University+Boulevard%2C+Ames%2C+IA!5e0!3m2!1sen!2sus!4v1390839289319" width="100%" height="300" frameborder="0"></iframe>
                                    </div>
                                    <div className="col-md-6">
                                        <ul className="list-unstyled p-0">
                                            <li>
                                                <b>Address</b>
                                                <p>Lorem Ipsum is simply dummy 
                                                text of the printing </p>
                                            </li>
                                            <li>
                                                <b>Phone</b>
                                                <p>123 456 7890</p>
                                            </li>
                                            <li>
                                                <b>Email</b>
                                                <p>sample@sample.com</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <form className="signup">
                                    <div className="form-group">
                                        <input type="text" className="form-control" name="campaignName" placeholder="First Name" />
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control" name="campaignTagline" placeholder="Last Name"/>
                                    </div>
                                    <div className="form-group">
                                        <input type="email" className="form-control" name="campaignName" placeholder="Email" />
                                    </div>
                                    <div className="form-group">
                                        <input type="email" className="form-control" name="campaignName" placeholder="Phone" />
                                    </div>
                                    <div className="form-group">
                                        <textarea class="form-control" rows="6" placeholder="Message"></textarea>
                                    </div>
                                    <div className="form-group">
                                        <button type="submit" className="btn btn-primary btn-raised px-3">Contact Now</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                 </section>
            </div>
           );
    }
}

export default Contact;

