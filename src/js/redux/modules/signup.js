import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import type { signupType } from '../../common/types/signup';

const SAVE_SIGNUP = 'app/signup/SAVE_SIGNUP';
const GET_SIGNUP = 'app/signup/GET_SIGNUP';

const SUBMIT_SIGNUP = 'app/signup/SUBMIT_SIGNUP';
const SIGNUP_RETURN = 'app/signup/SIGNUP_RETURN';

const GET_COUNTRIES = 'app/signup/GET_COUNTRIES';
const SET_COUNTRIES = 'app/signup/SET_COUNTRIES';

const GET_STATELIST = 'app/signup/GET_STATELIST';
const SET_STATELIST = 'app/signup/SET_STATELIST';

const GET_CITYLIST = 'app/signup/GET_CITYLIST';
const SET_CITYLIST = 'app/signup/SET_CITYLIST';

const SIGNUP_ERROR = 'app/login/SIGNUP_ERROR';

export const constants = {
  GET_SIGNUP,
  SAVE_SIGNUP,
  SUBMIT_SIGNUP,
  SIGNUP_RETURN,
  GET_COUNTRIES,
  SET_COUNTRIES,
  GET_STATELIST,
  SET_STATELIST,
  GET_CITYLIST,
  SET_CITYLIST,

  SIGNUP_ERROR
};

export const getSignup = createAction(GET_SIGNUP, () => ({}));
export const saveSignup = createAction(SAVE_SIGNUP, (result: signupType) => ({ result }));

export const submitSignup = createAction(SUBMIT_SIGNUP, (data:any) => ({data}));
export const signupReturn = createAction(SIGNUP_RETURN, (signup_return: any) => ({ signup_return }));

export const getCountries = createAction(GET_COUNTRIES, () => ({}));
export const setCountries = createAction(SET_COUNTRIES, (countries: signupType) => ({ countries }));

export const getStateList = createAction(GET_STATELIST, (id:any) => ({id}));
export const setStateList = createAction(SET_STATELIST, (states: signupType) => ({ states }));

export const getCityList = createAction(GET_CITYLIST, (id:any) => ({id}));
export const setCityList = createAction(SET_CITYLIST, (cities: signupType) => ({ cities }));

export const signupError = createAction(SIGNUP_ERROR, (signup_error: signupType) => ({ signup_error }));


export const actions = {
  getSignup,
  saveSignup,
  submitSignup,
  signupReturn,
  getCountries,
  setCountries,
  getStateList,
  setStateList,
  getCityList,
  setCityList,

  signupError
};
export const initialState = () =>
  Map({
    result: '',
    countries : '',
    states : '',
    cities : '',
    signup_return:'',
    signup_error:false,
  })

export const reducers = {
  [SAVE_SIGNUP]: (state, { payload }) =>
    state.merge({
      ...payload,
    }),
    [SIGNUP_RETURN]: (state, { payload }) =>
      state.merge({
        ...payload,
      }),
    [SET_COUNTRIES]: (state, { payload }) =>
      state.merge({
        ...payload,
      }),
      [SET_STATELIST]: (state, { payload }) =>
        state.merge({
          ...payload,
        }),
      [SET_STATELIST]: (state, { payload }) =>
        state.merge({
          ...payload,
        }),
        [SET_CITYLIST]: (state, { payload }) =>
          state.merge({
            ...payload,
          }),
          [SUBMIT_SIGNUP]: (state, { payload }) =>
          state.merge({
            ...payload,
          }),

          [SIGNUP_ERROR]:(state, { payload }) =>
          state.merge({
            ...payload,
          })
}

export default handleActions(reducers, initialState());
