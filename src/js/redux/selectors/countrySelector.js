import { createSelector } from 'reselect';

const countryDataSelector = state => state.country;

const resultSelector = createSelector(
  countryDataSelector,
  payload => payload.get('result')
);

export const countrySelector = state => ({
  countries: resultSelector(state),
});
