import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import LazyLoading from '../../common/components/LazyLoading/LazyLoading';
import { actions as editprofileActions } from '../../redux/modules/editprofile';
import { editprofileSelector } from '../../redux/selectors/editprofileSelector';
import { ExampleWithError } from '../../common/components/Example';
import { ErrorBoundary } from '../../common/components/Utilities';
const LazySample = Loadable({
  loader: () => import('../../common/components/Dashboard'),
  loading: LazyLoading,
 })
const mapDispatchToProps = {
  ...editprofileActions,
};
const mapStateToProps = state => ({
  editprofile: editprofileSelector(state),
});
@connect(mapStateToProps, mapDispatchToProps)

class Dashboard extends Component {
    static propTypes = {
      //home: PropTypes.object.isRequired,
    }

    // componentDidMount() {
    //   this.props.getEditProfile(111319);
    // }
    render() {
      return (
        <Fragment>
          <LazySample {...this.props} />
          <ErrorBoundary>
          <ExampleWithError {...this.props} />
        </ErrorBoundary>
        </Fragment>
      )
    }
}

export default Dashboard;
