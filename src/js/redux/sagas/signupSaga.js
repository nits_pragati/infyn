import { put, fork, takeLatest, call } from 'redux-saga/effects';
import { constants as signupConstants, actions as signupActions } from '../modules/signup';

import type { signupType } from '../../common/types/signup';
import axios from "axios";

function getFakeApi(){

  return axios({
        method: "get",
        url: "https://jsonplaceholder.typicode.com/posts/"
    });
}
export function* fetchHomeData() {
  // pretend there is an api call


  try {
          const response = yield call(getFakeApi);
          //const dog = response.data.message;
          console.log('checking sync 1',response);
          // dispatch a success action to the store with the new dog
          yield put(signupActions.saveSignup(response));

      } catch (error) {
          // dispatch a failure action to the store with the error
          //yield put({ type: "API_CALL_FAILURE", error });
          console.log('checking sync 1');
          yield put(signupActions.saveSignup(error));
      }

}



// export function* saveUserData(data) {
//   // pretend there is an api call

// console.log('APIDATA',data);
// return false;

// console.log("apiurl",data.payload);
//   const response = yield call(() => fetch(__CONFIG__.api_url+'user/add',
//                                           {method:'POST',
//                                           headers: {
//                                             "Content-type": "application/json"
//                                           },
//                                           body:JSON.stringify(data.payload)}).then(response => response.json()))
//   //
//   //console.log('bikash', response);
//   yield put(signupActions.signupReturn(response));
// }





function getSignupApi(data)
{
    var bodyFormData = new FormData();
    var req_data = data.data;
    for (var key in req_data) {
      if(key=='address' || key=='userAuth')
      {
        for(var k in req_data[key])
        {
          bodyFormData.append(key + '.' + k, req_data[key][k]);
        }
      }
      else {
        bodyFormData.append(key, req_data[key]);
      }



    }
    console.log('SIGNUP API BY FORM DATA BY SUMAN',bodyFormData);
  //__CONFIG__.api_url + "country"
  return axios({
        method: "POST",
        config: { headers: {'Content-Type': 'multipart/form-data' }},
        url:__CONFIG__.api_url + "user/add",
        data: bodyFormData
    }).then(response => response.data
    );
}

export function* getSignupList(actions) {
  console.log('SIGNUP PORTION',actions);
  try {
          const response = yield call(getSignupApi,actions.payload);
          //const dog = response.data.message;
          console.log('checking signup ',response);
          // dispatch a success action to the store with the new dog
          yield put(signupActions.signupReturn(response));

          yield put(signupActions.signupError(response));

      } catch (error) {
          // dispatch a failure action to the store with the error
          //yield put({ type: "API_CALL_FAILURE", error });
          console.log('checking signup 1');
          yield put(signupActions.signupReturn(error));

          yield put(signupActions.signupError(error));
      }

}




















function getCountriesApi()
{
  //__CONFIG__.api_url + "country"
  return axios({
        method: "get",
        url:__CONFIG__.api_url + "country"
    }).then(response => response.data
    );
}


export function* getCountryList() {
  console.log('checking sync ');
  try {
          const response = yield call(getCountriesApi);
          //const dog = response.data.message;
          console.log('checking sync ',response);
          // dispatch a success action to the store with the new dog
          yield put(signupActions.setCountries(response));

      } catch (error) {
          // dispatch a failure action to the store with the error
          //yield put({ type: "API_CALL_FAILURE", error });
          console.log('checking sync 1');
          yield put(signupActions.setCountries(error));
      }

}

function getStatesApi(params)
{
  //__CONFIG__.api_url + "country"
  console.log('state id ', params.id);
  return axios({
        method: "get",
        url:__CONFIG__.api_url + "state?countryId=" + params.id,
    }).then(response => response.data
    );
}

export function* getStateList(actions) {
  try {
          const response = yield call(getStatesApi,actions.payload);
          //const dog = response.data.message;
          console.log('checking states ',response);
          // dispatch a success action to the store with the new dog
          yield put(signupActions.setStateList(response));

      } catch (error) {
          // dispatch a failure action to the store with the error
          //yield put({ type: "API_CALL_FAILURE", error });
          console.log('checking states 1');
          yield put(signupActions.setStateList(error));
      }

}

function getCitiesApi(params)
{
  //__CONFIG__.api_url + "country"
  console.log('state id ', params.id);
  return axios({
        method: "get",
        url:__CONFIG__.api_url + "city?stateId=" + params.id,
    }).then(response => response.data
    );
}

export function* getCityList(actions) {
  try {
          const response = yield call(getCitiesApi,actions.payload);
          //const dog = response.data.message;
          console.log('checking states ',response);
          // dispatch a success action to the store with the new dog
          yield put(signupActions.setCityList(response));

      } catch (error) {
          // dispatch a failure action to the store with the error
          //yield put({ type: "API_CALL_FAILURE", error });
          console.log('checking states 1');
          yield put(signupActions.setCityList(error));
      }

}


function* watchGetSignup() {
  yield takeLatest(signupConstants.GET_SIGNUP, fetchHomeData);
  yield takeLatest(signupConstants.SUBMIT_SIGNUP, getSignupList);
  yield takeLatest(signupConstants.GET_COUNTRIES, getCountryList);
  yield takeLatest(signupConstants.GET_STATELIST, getStateList);
  yield takeLatest(signupConstants.GET_CITYLIST, getCityList);
}

export const signupSaga = [
  fork(watchGetSignup),
];
