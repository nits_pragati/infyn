import { put, fork, takeLatest, call } from 'redux-saga/effects';
import { constants as exampleConstants, actions as exampleActions } from '../modules/example';

import type { exampleType } from '../../common/types/example'
import axios from 'axios';

export function fetchUser() {
  console.log('API');
  // `axios` function returns promise, you can use any ajax lib, which can
  // return promise, or wrap in promise ajax call
  //return axios.post('http://111.93.169.90/bikash/swappaws/api/pets/petList.json');
}

export function* fetchExampleData() {
  // pretend there is an api call

  // const result: exampleType = {
  //   title: 'Everything is Awesome',
  //   description: __CONFIG__.description,
  //   source: 'This message is coming from Redux',
  // };
  // axios.post('http://166.62.40.135:9000/ForDonor-0.0.1-SNAPSHOT/api/campaign/showall').then(res => {
  //    console.log(res);
  //   // const result = res.data;
  //   yield put(setRecords(res));
  // }).catch(err => {
  //   console.log(err);
  // });

  // try {
  //   const response = call fetch('http://166.62.40.135:9000/ForDonor-0.0.1-SNAPSHOT/api/campaign/showall');
  //   const responseBody = response.json();
  //   console.log(responseBody)
  //   yield put(setRecords(responseBody));
  // } catch (e) {
  //   yield put(fetchFailed(e));
  //   //return;
  // }

  
  // const result: exampleType = {
    
  // };

  const userData = yield call(fetchUser);
  // Instructing middleware to dispatch corresponding action.
  
  console.log('in function ', userData);
  yield put(exampleActions.updateExample(userData.data));
}




function* watchGetExample() {
  yield takeLatest(exampleConstants.GET_EXAMPLE, fetchExampleData);
}

export const exampleSaga = [
  fork(watchGetExample),
];
