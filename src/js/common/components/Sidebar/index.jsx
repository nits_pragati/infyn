import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { actions as loginActions } from '../../../redux/modules/login';
import { loginSelector } from '../../../redux/selectors/loginSelector';
import { connect } from 'react-redux';

class Sidebar extends PureComponent {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);

    }
    logout(){
      this.props.userLogout();
      this.props.history.push("/");
    }



    render() {
        return (
          <div className="col-md-3">
              <div className="list-group p-0 mb-3">
                  <Link to="/dashboard" className="list-group-item list-group-item-action ">Dashboard</Link>
                    <Link to="/profile" className="list-group-item list-group-item-action ">Profile</Link>
                  {/* <a href="#" class="list-group-item list-group-item-action">Profile</a> */}
                  <Link to="/verification" className="list-group-item list-group-item-action">Verification</Link>
                  {/* <a href="#" className="list-group-item list-group-item-action">My Campaigns</a> */}
                  {/* <a href="#" class="list-group-item list-group-item-action">Contributions</a>
                  <a href="#" class="list-group-item list-group-item-action">Comments</a>
                  <a href="#" class="list-group-item list-group-item-action">Settings</a> */}
                  <a onClick={this.logout} className="list-group-item list-group-item-action">logout</a>
              </div>
          </div>
        );
      }
}

const mapDispatchToProps = {
  ...loginActions,
};
const mapStateToProps = state => ({
  login: loginSelector(state),
});
export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
