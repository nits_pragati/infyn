import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import { loginType } from '../../common/types/login';


const SUBMIT_LOGIN = 'app/login/SUBMIT_LOGIN';
const LOGIN_RETURN = 'app/login/LOGIN_RETURN';
const LOGIN_ERROR = 'app/login/LOGIN_ERROR';
const IS_LOGGED_IN = 'app/login/IS_LOGGED_IN';
const SET_LOGIN_DETAILS = 'app/login/SET_LOGIN_DETAILS';
const USER_LOGOUT = 'app/login/USER_LOGOUT';

export const constants = {
  SUBMIT_LOGIN,
  LOGIN_RETURN,
  LOGIN_ERROR,
  IS_LOGGED_IN,
  SET_LOGIN_DETAILS,
  USER_LOGOUT
};


export const submitLogin = createAction(SUBMIT_LOGIN, (data: any) => ({data}));
export const loginReturn = createAction(LOGIN_RETURN, (result: any) => ({ result }));
export const loginError = createAction(LOGIN_ERROR, (login_error: loginType) => ({ login_error }));
export const isLoggedIn = createAction(IS_LOGGED_IN, () => ({  }));
export const setLoginDetails = createAction(SET_LOGIN_DETAILS, (userData: any) => ({userData}));
export const userLogout = createAction(USER_LOGOUT, () => ({}));



export const actions = {
  submitLogin,
  loginReturn,
  loginError,
  isLoggedIn,
  setLoginDetails,
  userLogout
};
export const initialState = () =>
  Map({
    result: '',
    login_error:false,
    userData:false
  })

export const reducers = {

    [LOGIN_RETURN]: (state, { payload }) =>
      state.merge({
        ...payload,
      }),
      [LOGIN_ERROR]:(state, { payload }) =>
        state.merge({
          ...payload,
        }),
        [SET_LOGIN_DETAILS]:(state, { payload }) =>
          state.merge({
            ...payload,
          }),
}

export default handleActions(reducers, initialState());
