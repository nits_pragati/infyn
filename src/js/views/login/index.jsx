import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import LazyLoading from '../../common/components/LazyLoading/LazyLoading';
import { actions as loginActions } from '../../redux/modules/login';
import { loginSelector } from '../../redux/selectors/loginSelector';
// import { ExampleWithError } from '../../common/components/Example';
// import { ErrorBoundary } from '../../common/components/Utilities';
const LazySample = Loadable({
  loader: () => import('../../common/components/Login'),
  loading: LazyLoading,
})
const mapDispatchToProps = {
  ...loginActions,
};
const mapStateToProps = state => ({
  login: loginSelector(state),
});
@connect(mapStateToProps, mapDispatchToProps)
// @connect(mapDispatchToProps)
class LoginView extends Component {

    // static propTypes = {
    //   submitLogin: PropTypes.func,
    // }

    // componentDidMount() {
    // this.props.getSignup();
    // }
    render() {
      return (
        <Fragment>
          <LazySample {...this.props} />
        </Fragment>
      )
    }
}

export default LoginView;
