import React, { PureComponent } from 'react';
import { getAboutUsDetails } from '../../api/commonApi';
import { NotificationManager } from 'react-notifications';
class AboutUs extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            description: ''
        }
    }


    componentDidMount() {
        getAboutUsDetails().then((res) => {
            if (res.data) {
                this.setState({ description: res.data.content });
            }
        }).catch((err) => {
            NotificationManager.error("Please try again later.", "Error");
        })



    }
    returnHtml() {
        return { __html: this.state.description };
    }


    render() {
        return (
            <div >
                {
                    this.state.description ? (
                        <div dangerouslySetInnerHTML={this.returnHtml()}></div>
                    ) : (
                            <div>Hello</div>
                        )
                }
                {/* <section className="inner-banner aboutClass">

                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9 col-md-9 ml-auto mr-auto">
                                <div className="home-banner-text">

                                    <img src={require('../../../../assets/img/about-icon.png')} />
                                    <h2>About Us</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div className="inner-body my-4">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-2">
                            </div>
                            <div className="col-md-8 col-12">
                                <div className="about-text my-4">
                                    <h2>About Us</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
												printer took.Consec adipiscing elit sed diam nonummy ni euismod tinc.</p>
                                    <p><i className="fa fa-check pr-2"></i>High Qualified Professionals</p>
                                    <p><i className="fa fa-check pr-2"></i>Affordable Prices</p>
                                    <p><i className="fa fa-check pr-2"></i>Successful Efforts</p>
                                    <button type="button" className="btn-primery">Book An Appointment</button>
                                </div>
                            </div>
                            <div className="col-md-2">
                            </div>
                        </div>
                    </div>
                </div> */}
            </div>




        )
    }
}


export default AboutUs;
